import sessionTraining
import tensorflow as tf
import numpy as np
import math

runner = sessionTraining.Runner()

file_labelCount = open("nLabels.log","r") 
for line in file_labelCount.readlines():
	labelCount = int(line)
	
file_features_training = open("features_training.log","r") 
file_features_testing = open("features_testing.log","r") 
file_features_val = open("features_val.log","r") 
file_features_human = open("features_human.log","r") 
file_labels_training = open("master_labels_training.log","r")
file_labels_testing = open("master_labels_testing.log","r")
file_labels_val = open("master_labels_val.log","r")
file_labels_human = open("master_labels_human.log","r")

array_features_train = []
array_outputs_train = []
array_features_test = []
array_outputs_test = []
array_features_val = []
array_outputs_val = []
array_features_human = []
array_outputs_human = []

array_features_train_local = []
array_features_test_local = []
array_features_human_local = []


	
for line in file_features_training.readlines():
	split = line.split(" ")
	#split.remove("\n")
	array = []
	for x in split:
		f = float(x)
		array.append(f)
	array_features_train.append(array)
	
for line in file_features_testing.readlines():
	split = line.split(" ")
	#split.remove("\n")
	array = []
	for x in split:
		f = float(x)
		array.append(f)
	array_features_test.append(array)
	
for line in file_features_val.readlines():
	split = line.split(" ")
	#split.remove("\n")
	array = []
	for x in split:
		f = float(x)
		array.append(f)
	array_features_val.append(array)
	
for line in file_features_human.readlines():
	split = line.split(" ")
	#split.remove("\n")
	array = []
	for x in split:
		f = float(x)
		array.append(f)
	array_features_human.append(array)


maxLabel = 0
for line in file_labels_training.readlines():
	split = line.split(" ")
	array = []
	for x in split:
		if (len(line) == 0):
			pass
		elif (x == "\n"):
			pass
		else:
			f = int(x)
			if f > maxLabel:
				maxLabel = f
			array.append(f)
	array_outputs_train.append(array)

		
for line in file_labels_testing.readlines():
	split = line.split(" ")
	array = []
	for x in split:
		if (len(line) == 0):
			pass
		elif (x == "\n"):
			pass
		else:
			f = int(x)
			if f > maxLabel:
				maxLabel = f
			array.append(f)
	array_outputs_test.append(array)

		
for line in file_labels_val.readlines():
	split = line.split(" ")
	array = []
	for x in split:
		if (len(line) == 0):
			pass
		elif (x == "\n"):
			pass
		else:
			f = int(x)
			if f > maxLabel:
				maxLabel = f
			array.append(f)
	array_outputs_val.append(array)
	
for line in file_labels_human.readlines():
	split = line.split(" ")
	array = []
	for x in split:
		if (len(line) == 0):
			pass
		elif (x == "\n"):
			pass
		else:
			f = int(x)
			if f > maxLabel:
				maxLabel = f
			array.append(f)
	array_outputs_human.append(array)

print(str(len(array_outputs_test)) + " " + str(len(array_outputs_train)))
	
nBatches = len(array_features_train)
vector_size = len(array_features_train[0])
print("n data points: " + str(nBatches))
print("Vector size: " + str(vector_size))
print("Max label: " + str(labelCount))

input_values = np.zeros((len(array_features_train),len(array_features_train[0])))
input_values[:] = array_features_train[:]
test_input_values = np.zeros((len(array_features_test),len(array_features_test[0])))
test_input_values[:] = array_features_test[:]
val_input_values = np.zeros((len(array_features_val),len(array_features_val[0])))
val_input_values[:] = array_features_val[:]
human_input_values = np.zeros((len(array_features_human),len(array_features_human[0])))
human_input_values[:] = array_features_human[:]

output_values_set = []
val_output_values_set = []
test_output_values_set = []
human_output_values_set = []
for lIndex in range(len(array_outputs_train[0])):
	output_values = np.zeros((len(array_outputs_train),1))
	for i in range(len(array_outputs_train)):
		output_values[i][0] = array_outputs_train[i][lIndex]
	output_values_set.append(output_values)
		
	test_output_values = np.zeros((len(array_outputs_test),1))
	for i in range(len(array_outputs_test)):
		test_output_values[i][0] = array_outputs_test[i][lIndex]
	test_output_values_set.append(test_output_values)
		
	val_output_values = np.zeros((len(array_outputs_val),1))
	for i in range(len(array_outputs_val)):
		val_output_values[i][0] = array_outputs_val[i][lIndex]
	val_output_values_set.append(val_output_values)
	
	human_output_values = np.zeros((len(array_outputs_human),1))
	for i in range(len(array_outputs_human)):
		human_output_values[i][0] = array_outputs_human[i][lIndex]
	human_output_values_set.append(human_output_values)

print("Shapes: " + str(input_values.shape) + " "+ str(val_input_values.shape) + " "+ str(output_values_set[0].shape) + " "+ str(val_output_values_set[0].shape) + " " )

runner.run_session(input_values,output_values_set,val_input_values,val_output_values_set,test_input_values,test_output_values_set,human_input_values,human_output_values_set,labelCount)