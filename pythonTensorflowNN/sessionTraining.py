import tensorflow as tf
import numpy as np


class Runner:
	#Hyperparams
	useDropout = True
	dropoutKeepProb = 0.75
	nDenseLayers = 8
	nNodes = 512
	earlyStopping = 32
	
	##----Defined on runtime-----
	output_size = 1
	number_of_labels = 2
	vectorSize = 0
	batchSize = 0
	
	
	def run_session(self,
					input_values,
					output_values_set,
					val_in_values,
					val_out_values_set,
					test_in_values,
					test_out_values_set,
					human_in_values,
					human_out_values_set,
					maxLabel,
					optimizer=tf.train.RMSPropOptimizer(learning_rate=1e-5, momentum=0.9)
					#optimizer=tf.train.AdamOptimizer()
					):
		self.vectorSize = input_values.shape[1]
		self.batchSize = input_values.shape[0]
		self.number_of_labels = maxLabel
		output_size = len(output_values_set)
		print("Output layer count: " + str(output_size))
		
		inputArray = []
		encodedMemory = []
		yPlaceholders = []
		
		self.x_shape = [None, self.vectorSize]
		self.y_shape = [None, 1]
		for i in range(output_size):
			y = tf.placeholder(tf.int32, self.y_shape, name="Y_" + str(i))#Index of correct class
			yPlaceholders.append(y)
		
		x = tf.placeholder(tf.float32, self.x_shape, name="X")
		keep_prob = tf.placeholder("float",name="dropout_prob_placeholder")
		prevLayer = x
			
			
		#Rescale it to the right size to allow res layers
		dense = tf.layers.dense(inputs=prevLayer, units=self.nNodes, activation=tf.nn.relu)
		prevLayer = dense
			
		for i in range(self.nDenseLayers):
			residualFirstLayer = prevLayer
			##Res 1
			if (self.useDropout):
				prevLayer = tf.nn.dropout(prevLayer, keep_prob)#Dropout layer
			dense = tf.layers.dense(inputs=prevLayer, units=self.nNodes, activation=tf.nn.relu)
			prevLayer = dense
			##Res 2
			if (self.useDropout):
				prevLayer = tf.nn.dropout(prevLayer, keep_prob)#Dropout layer
			dense = tf.layers.dense(inputs=prevLayer, units=self.nNodes, activation=tf.nn.relu)
			prevLayer = dense
			
			prevLayer = tf.math.add(residualFirstLayer,prevLayer)
		
		print("Defining training operations... Size " + str(output_size))
		train_op_set = []
		losses = []
		output_layers = []
		for i in range(output_size):
			# outputLayer. Logits form, to allow single integer adressing from labels
			logits = tf.layers.dense(inputs=prevLayer, units=self.number_of_labels,name="Logits_" + str(i))
			output_layers.append(logits)
			loss = tf.losses.sparse_softmax_cross_entropy(labels=yPlaceholders[i], logits=logits)
			losses.append(loss)
			train_op_local = optimizer.minimize(loss=loss,global_step=tf.train.get_global_step())
			train_op_set.append(train_op_local)
		train_op = tf.group(train_op_set)
		loss = losses[0]#This is the default one to return the cost value of.
		print("Training operations defined.")
		
		from time import time
		t = time()
		bestCostValue = 100000
		bestAccSeen = 0
		humanAtBest = 0
		saver = tf.train.Saver()
		costStringBuffer = []
		accuracyBuffer = []
		earlyStopClock = 0
		maxSteps = 2500
		print("Starting session...")
		with tf.Session() as sess:
			tf.global_variables_initializer().run()
			
			bestPredictionsTrain = None
			bestPredictionsTest = None
			bestPredictionsVal = None
			bestPredictionsHuman = None
			for step in range(maxSteps):
				print("Starting step " + str(step) + "...")
				feedDictionary = {}
				for j in range(output_size):
					feedDictionary[yPlaceholders[j]]  = output_values_set[j]
				feedDictionary[x] = input_values
				feedDictionary[keep_prob] = self.dropoutKeepProb
				ops = []
				ops.append(train_op)
				ops.append(loss)
				outValueSet = sess.run(ops,feed_dict=feedDictionary)
				
				
				if (step % 5 == 0):
					accuracyString = ""
					###Assess each layer's effectiveness, in terms of costs, accuracy and predictions for saving to disk
					feedDictionary = {}
					for j in range(output_size):
						feedDictionary[yPlaceholders[j]]  = output_values_set[j]
					feedDictionary[x] = input_values
					feedDictionary[keep_prob] = 1
					opList = []
					for j in range(output_size):
						opList.append(output_layers[j])
					for j in range(output_size):
						opList.append(losses[j])
					predictions_and_costs = sess.run(opList,feed_dict=feedDictionary)
					predictionSet_training = []
					for j in range(output_size):
						#acc = self.getAccuracy(output_values_set[j],predictions_and_costs[j])
						#print("Prediction layer Train: " + str(j) + " " +  str(acc))
						#accuracyString += str(acc) + " "
						predictionSet_training.append(predictions_and_costs[j])
					costSet = []
					for j in range(output_size):
						costSet.append(predictions_and_costs[j + output_size])
					cost_value = costSet[0]
					
					##Validation repeat
					feedDictionary = {}
					for j in range(output_size):
						feedDictionary[yPlaceholders[j]]  = val_out_values_set[j]
					feedDictionary[x] = val_in_values
					feedDictionary[keep_prob] = 1
					opList = []
					for j in range(output_size):
						opList.append(output_layers[j])
					for j in range(output_size):
						opList.append(losses[j])
					predictions_and_costs = sess.run(opList,feed_dict=feedDictionary)
					predictionSet_val = []
					for j in range(output_size):
						#acc = self.getAccuracy(val_out_values_set[j],predictions_and_costs[j])
						#accuracyString += str(acc) + " "
						#print("Prediction layer Validate: " + str(j) + " " +  str(acc))
						predictionSet_val.append(predictions_and_costs[j])
					costSet = []
					for j in range(output_size):
						costSet.append(predictions_and_costs[j + output_size])
					val_cost_value = costSet[0]
					
					##Testing repeat
					feedDictionary = {}
					for j in range(output_size):
						feedDictionary[yPlaceholders[j]]  = test_out_values_set[j]
					feedDictionary[x] = test_in_values
					feedDictionary[keep_prob] = 1
					opList = []
					for j in range(output_size):
						opList.append(output_layers[j])
					for j in range(output_size):
						opList.append(losses[j])
					predictions_and_costs = sess.run(opList,feed_dict=feedDictionary)
					predictionSet_testing = []
					for j in range(output_size):
						#acc = self.getAccuracy(val_out_values_set[j],predictions_and_costs[j])
						#accuracyString += str(acc) + " "
						#print("Prediction layer Validate: " + str(j) + " " +  str(acc))
						predictionSet_testing.append(predictions_and_costs[j])
					costSet = []
					for j in range(output_size):
						costSet.append(predictions_and_costs[j + output_size])
					test_cost_value = costSet[0]
					
					##Human repeat
					feedDictionary = {}
					for j in range(output_size):
						feedDictionary[yPlaceholders[j]]  = human_out_values_set[j]
					feedDictionary[x] = human_in_values
					feedDictionary[keep_prob] = 1
					opList = []
					for j in range(output_size):
						opList.append(output_layers[j])
					for j in range(output_size):
						opList.append(losses[j])
					predictions_and_costs = sess.run(opList,feed_dict=feedDictionary)
					predictionSet_human = []
					for j in range(output_size):
						#acc = self.getAccuracy(val_out_values_set[j],predictions_and_costs[j])
						#accuracyString += str(acc) + " "
						#print("Prediction layer Validate: " + str(j) + " " +  str(acc))
						predictionSet_human.append(predictions_and_costs[j])
					costSet = []
					for j in range(output_size):
						costSet.append(predictions_and_costs[j + output_size])
					human_cost_value = costSet[0]
						
					costStr = "Step " + str(step) + "/" + str(maxSteps) + " General_Cost %.4f Human_Cost %.4f Val_Cost %.4f Test_Cost %.4f BestEver %.4f HumanAtBest %.4f Time(s) %.2f" % (cost_value,human_cost_value,val_cost_value,test_cost_value, bestCostValue,humanAtBest,time() - t)
					print(costStr)
					costStringBuffer.append(costStr)
					file = open("costs.log","w")
					for line in costStringBuffer:
						file.write(line + "\n")
					file.close()
					t = time()
					
					#accuracyBuffer.append(accuracyString)
					#file = open("accuracies.log","w")
					#for line in accuracyBuffer:
					#	file.write(line + "\n")
					#file.close()
					
					if val_cost_value < bestCostValue:
						print("Caching best seen...")
						earlyStopClock = 0
						bestCostValue = val_cost_value
						humanAtBest = human_cost_value
						bestPredictionsTrain = []
						bestPredictionsTest = []
							
						for prediction in predictionSet_training:
							bestPredictionsTrain.append(prediction[:])
						for prediction in predictionSet_testing:
							bestPredictionsTest.append(prediction[:])
					else:
						earlyStopClock += 1
						if (earlyStopClock > self.earlyStopping):
							print("Early stopping hit")
							break
						
			print("Saving best seen...")
			
			
			file = open("nn_weightings_training.log","w")
			for i in range(predictionSet_training[0].shape[0]):
				for prediction in predictionSet_training:
					line = ""
					for j in range(prediction.shape[1]):
						line += str(prediction[i][j]) + " "
					file.write(line + "\n")
			file.close()
			print("Wrote out nn weightings training")
			
			file = open("nn_weightings_validation.log","w")
			for i in range(predictionSet_val[0].shape[0]):
				for prediction in predictionSet_val:
					line = ""
					for j in range(prediction.shape[1]):
						line += str(prediction[i][j]) + " "
					file.write(line + "\n")
			file.close()
			print("Wrote out nn weightings validation")
			
			file = open("nn_weightings_testing.log","w")
			for i in range(predictionSet_testing[0].shape[0]):
				for prediction in predictionSet_testing:
					line = ""
					for j in range(prediction.shape[1]):
						line += str(prediction[i][j]) + " "
					file.write(line + "\n")
			file.close()
			print("Wrote out nn weightings testing")
			
			
			file = open("nn_weightings_human.log","w")
			for i in range(predictionSet_human[0].shape[0]):
				for prediction in predictionSet_human:
					line = ""
					for j in range(prediction.shape[1]):
						line += str(prediction[i][j]) + " "
					file.write(line + "\n")
			file.close()
			print("Wrote out nn weightings human")
			
			
			
			'''
			print("Saving variables to disk...")
			for var in tf.trainable_variables():
				nameStr = str(var.name).replace("/","%")
				nameStr = nameStr.replace(":","_")
				print("Saving Variable Parameters:" + nameStr)
				fileName = "savedNN/params_" + nameStr + ".npz"
				npArray = sess.run(var)
				np.savez(fileName,sess.run(var))
			print("Save complete")
			'''
							
					
					
			print("Training complete.")
			
	def getAccuracy(self,values,predictions):
		score = 0.0
		for i in range(values.shape[0]):
			highestV = predictions[i][0]
			highestI = 0
			for j in range(predictions.shape[1]):
				if (predictions[i][j] > highestV):
					highestV = predictions[i][j]
					highestI = j
			if (values[i] == highestI):
				score += 1.0
		return score/values.shape[0]