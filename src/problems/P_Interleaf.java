package problems;

public class P_Interleaf extends Problem{

	@Override
	public int[] run(int[] input,int param) {
		int[] reply = new int[input.length];
		int halfway = input.length/2;
		for (int i=0;i<halfway;i++) {
			reply[i*2] = input[i];
		}
		for (int i=0;i<halfway;i++) {
			reply[i*2 + 1] = input[i+halfway];
		}
		return reply;
	}

	@Override
	public int[] asSimple(int[] input,int param) {
		int[] reply;
		int halfway;
		int multTwo;
		int offset;
		int inputLen;
		int i;
		boolean cond;
		
		i = 0;
		inputLen = input.length;
		reply = new int[inputLen];
		halfway = input.length/2;
		
		cond = i < halfway;
		while(cond) {
			multTwo = i*2;
			reply[multTwo] = input[i];
			i = i + 1;
			cond = i < halfway;
		}
		
		i = 0;
		cond = i < halfway;
		while(cond) {
			multTwo = i*2;
			multTwo = multTwo + 1;
			offset = halfway + i;
			reply[multTwo] = input[offset];
			i = i + 1;
			cond = i < halfway;
		}
		
		return reply;
	}
}
