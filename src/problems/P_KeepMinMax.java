package problems;

public class P_KeepMinMax extends Problem{

	@Override
	public int[] run(int[] input,int param) {
		int[] reply = new int[input.length];
		int max = input[0];
		int min = input[0];
		for (int i=0;i<reply.length;i++) {
			if (input[i] > max) {max = input[i];}
			if (input[i] < min) {min = input[i];}
		}
		for (int i=0;i<reply.length;i++) {
			if (input[i] == max) {reply[i] = input[i];}
			if (input[i] == min) {reply[i] = input[i];}
		}
		
		return reply;
	}

	@Override
	public int[] asSimple(int[] input,int param) {
		int[] reply;

		int i;
		int inputLen;
		int min;
		int max;
		boolean cond;
		
		min = input[0];
		max = input[0];
		inputLen = input.length;
		reply = new int[inputLen];

		i = 0;
		cond = i < inputLen;
		while(cond) {
			cond = input[i] > max;
			if (cond) {
				max = input[i];
			}
			cond = input[i] < min;
			if (cond) {
				min = input[i];
			}
			
			i = i + 1;
			cond = i < inputLen;
		}

		i = 0;
		cond = i < inputLen;
		while(cond) {
			cond = input[i] == max;
			if (cond) {
				reply[i] = input[i];
			}
			cond = input[i] == min;
			if (cond) {
				reply[i] = input[i];
			}
			
			i = i + 1;
			cond = i < inputLen;
		}
		return reply;
	}
}
 