package problems;

public class P2_Sort extends Problem2{
	public int[] run(int[] input) {
		int[] reply = new int[input.length];

		for (int i=0;i<input.length;i++) {reply[i] = input[i];}
		for (int i=0;i<reply.length;i++) {
			for (int j=0;j<reply.length-1;j++) {
				if (reply[j] < reply[j+1]) {
					int swap = reply[j];
					reply[j] = reply[j+1];
					reply[j+1] = swap;
				}
			}
		}
		
		return reply;
	}
}
