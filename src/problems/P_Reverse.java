package problems;

public class P_Reverse extends Problem{

	@Override
	public int[] run(int[] input,int param) {
		int[] reply = new int[input.length];
		for (int i=0;i<input.length;i++) {
			reply[i] = input[input.length-i-1];
		}
		return reply;
	}

}
