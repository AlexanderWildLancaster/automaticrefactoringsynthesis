package problems;

public class P2_Reverse extends Problem2{

	@Override
	public int[] run(int[] input) {
		int[] reply = new int[input.length];
		for (int i=0;i<input.length;i++) {
			reply[i] = input[input.length-i-1];
		}
		return reply;
	}

}
