package problems;

public class P_OffsetFibbonaci extends Problem{

	@Override
	public int[] run(int[] input,int param) {
		int[] reply = new int[input.length];
		int nIndices = param + input.length;
		int q = 0;
		int a = 0;
		int b = 1;
		for (int i=0;i<nIndices;i++) {
			int next = a + b;
			a = b;
			b = next;
			if (i >= param) {
				reply[q] = a;
				q += 1;
			}
		}
		return reply;
	}

	@Override
	public int[] asSimple(int[] input,int param) {
		int[] reply;
		int nIndices;
		int inputLen;
		int q;
		int a;
		int b;
		int next;
		int i;
		boolean cond;
		
		inputLen = input.length;
		reply = new int[inputLen];
		nIndices = inputLen + param;
		q = 0;
		a = 0;
		b = 1;
		
		i = 0;
		cond = i < nIndices;
		while(cond) {
			next = a + b;
			a = b;
			b = next;
			cond = i >= param;
			if (cond) {
				reply[q] = a;
				q = q + 1;
			}
			i = i + 1;
			cond = i < nIndices;
		}
		
		return reply;
	}
}
