package problems;

public class P2_AbsValue extends Problem2{

	@Override
	public int[] run(int[] input) {
		int[] reply = new int[input.length];
		for (int i=0;i<reply.length;i++){
			input[i] = Math.abs(reply[i]);
		}
		return reply;
	}
}
