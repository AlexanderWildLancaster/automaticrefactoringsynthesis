package problems;

public class P_RemoveDupesKeepFirst extends Problem{

	@Override
	public int[] run(int[] input,int param) {
		int[] reply = new int[input.length];
		for (int i=0;i<reply.length;i++) {
			for (int j=i+1;j<reply.length;j++) {
				if (input[i] == input[j]) {
					input[j] = 0;
				}
			}
			reply[i] = input[i];
		}
		return reply;
	}

}
 