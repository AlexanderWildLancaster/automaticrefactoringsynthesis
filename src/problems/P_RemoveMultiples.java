package problems;

public class P_RemoveMultiples extends Problem{

	@Override
	public int[] run(int[] input, int param) {
		int[] reply = new int[input.length];
		for (int i=0;i<reply.length;i++) {
			if (input[i] % param == 0) {reply[i] = 0;}
			else {reply[i] = input[i];}
		}
		return reply;
	}

}
