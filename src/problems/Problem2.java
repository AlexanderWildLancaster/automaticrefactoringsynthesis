package problems;

import main.Utils;

public abstract class Problem2 {
	public abstract int[] run(int[] input);
	public int[] randomInput(int len) {
		int[] reply = new int[len];
		for (int i=0;i<reply.length;i++) {
			reply[i] = Utils.random.nextInt(16) - 8;
		}
		return reply;
	}
}
