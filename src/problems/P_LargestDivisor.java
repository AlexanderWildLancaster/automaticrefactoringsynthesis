package problems;

public class P_LargestDivisor extends Problem{

	@Override
	public int[] run(int[] input,int param) {
		int[] reply = new int[input.length];
		for (int i=0;i<reply.length;i++) {
			int factor = 0;
			for (int j=1;j<input[i]-1;j++) {
				if (input[i] % j == 0) {
					factor = j;
				}
			}
			reply[i] = factor;
		}
		return reply;
	}
	
	@Override
	public int[] asSimple(int[] input,int param) {
		int[] reply;

		int j;
		int i;
		int hold;
		int inputLen;
		int shortLen;
		int factor;
		boolean cond;
		
		inputLen = input.length;
		shortLen = inputLen -1;
		reply = new int[inputLen];

		i = 0;
		cond = i < inputLen;
		while(cond) {
			reply[i] = input[i];
			factor = 0;
			j = 1;
			cond = j < shortLen;
			while(cond) {
				hold = input[i] % j;
				cond = hold == 0;
				if (cond) {
					factor = j;
				}
				j = j + 1;
				cond = j < shortLen;
			}
			
			reply[i] = factor;
			i = i + 1;
			cond = i < inputLen;
		}

		return reply;
	}

}
