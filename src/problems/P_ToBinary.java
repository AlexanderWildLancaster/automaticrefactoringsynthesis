package problems;

public class P_ToBinary extends Problem{

	@Override
	public int[] run(int[] input,int param) {
		int[] reply = new int[input.length];
		if (param < 0) {reply[reply.length-1] = 1;}
		for (int i=0;i<input.length;i++) {
			if (param % 2 != 0) {
				reply[i] = 1;
			}
			param /= 2;
		}
		return reply;
	}

}
