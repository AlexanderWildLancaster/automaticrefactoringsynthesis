package main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class LogBox {
	public String filename;
	public ArrayList<String> allLines = new ArrayList<String>();

	public LogBox(String name) {
		filename = name + ".log";
	}
	public LogBox(String name,String episodeName) {
		filename = name + "_" + episodeName + ".log";
	}

	public void takeLine(String line) {
		File f = new File(filename);
		//Remove previous file if one exists and this is your first line
		if (allLines.size() == 0) {
			if (f.exists()) {
				f.delete();
			}
		}

		allLines.add(line);
		if (f.exists() == false) {
			FileWriter w = null;
			try {
				w = new FileWriter(f);
				for (String s:allLines) {
					w.write(s + "\n");
				}
				w.close();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					w.close();
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		else {
			FileWriter w = null;
			try {
				w = new FileWriter(f,true);
				w.write(line + "\n");
				w.close();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					w.close();
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
