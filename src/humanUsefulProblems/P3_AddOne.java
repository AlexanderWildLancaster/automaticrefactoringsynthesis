package humanUsefulProblems;

import java.util.ArrayList;

import main.Gridlang;

public class P3_AddOne extends Problem3{

	@Override
	public int[] process(int[] input, int param) {
		int[] out = input.clone();
		for (int i=0;i<out.length;i++) {
			out[i] = input[i] + 1;
		}
		return out;
	}

	@Override
	public int[][] getGridlang(int nLines) {
		ArrayList<int[]> lines = new ArrayList<int[]>();
		lines.add(new int[] {Gridlang.OP_VAR_TO_LITERAL,3,1,0,0});//Var_3 to literal 1
		lines.add(new int[] {Gridlang.OP_LOOP,2,0,0,0});//iterator to 2
		lines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,2,4,0});//Add var_4 to in[var_2]
		lines.add(new int[] {Gridlang.OP_ADD,4,4,3,0});//var_4 = var_4 + var_3
		lines.add(new int[] {Gridlang.OP_VAR_TO_ARRAY_ASSIGN,1,2,4,0});//Add var_4 to out[var_2]
		lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endloop
		while(lines.size() < nLines) {lines.add(new int[] {0,0,0,0,0});}
		
		int[][] program = new int[nLines][5];
		for (int i=0;i<program.length;i++) {
			for (int j=0;j<5;j++) {
				program[i][j] = lines.get(i)[j];
			}
		}
		return program;
	}

}
