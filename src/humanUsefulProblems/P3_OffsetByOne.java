package humanUsefulProblems;

import java.util.ArrayList;

import main.Gridlang;

public class P3_OffsetByOne extends Problem3{

	@Override
	public int[] process(int[] input, int param) {
		int[] out = new int[input.length];
		for (int i=0;i<out.length-1;i++) {
			out[i+1] = input[i];
		}
		out[0] = input[7];
		return out;
	}

	@Override
	public int[][] getGridlang(int nLines) {
		ArrayList<int[]> lines = new ArrayList<int[]>();
		lines.add(new int[] {Gridlang.OP_VAR_TO_LITERAL,4,1,0,0});//var_4 = 1
		lines.add(new int[] {Gridlang.OP_SUBTRACT,5,0,4,0});//var_5 = arrayLen - var_4
		lines.add(new int[] {Gridlang.OP_LOOP,2,5,0,0});//iterator to 2
		lines.add(new int[] {Gridlang.OP_ADD,3,2,4,0});//var_3 = var_2 + var_4
		lines.add(new int[] {Gridlang.OP_ARRAY_TO_ARRAY_ASSIGN,1,3,0,2});
		lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endloop
		lines.add(new int[] {Gridlang.OP_VAR_TO_LITERAL,3,0,0,0});//var_3 = 0
		lines.add(new int[] {Gridlang.OP_ARRAY_TO_ARRAY_ASSIGN,1,3,0,2});
		while(lines.size() < nLines) {lines.add(new int[] {0,0,0,0,0});}
		
		int[][] program = new int[nLines][5];
		for (int i=0;i<program.length;i++) {
			for (int j=0;j<5;j++) {
				program[i][j] = lines.get(i)[j];
			}
		}
		return program;
	}

}
