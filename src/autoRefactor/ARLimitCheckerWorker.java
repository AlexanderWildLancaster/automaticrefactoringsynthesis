package autoRefactor;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.concurrent.Callable;

public class ARLimitCheckerWorker  implements Callable<ARLimitCheckerWorker>{
		public ARLimitCheckerWorker worker;
//		ArrayList<double[]> nnWeightings;
		ArrayList<int[]> targetPrograms;
		ArrayList<double[]> responses;
		ArrayList<String> responseLog;
		ArrayList<Integer> indices;
		private int trainingFlag;
		
	public ARLimitCheckerWorker(
//			ArrayList<double[]> nnWeightings
			int trainingFlag,
			ArrayList<int[]> targetPrograms
			,ArrayList<double[]> responses
			,ArrayList<Integer> indices
			,ArrayList<String> responseLog
			) {
//		this.nnWeightings = nnWeightings;
		this.trainingFlag = trainingFlag;
		this.targetPrograms = targetPrograms;
		this.responses = responses;
		this.indices = indices;
		this.responseLog = responseLog;
		this.worker = this;
	}

	@Override
	public ARLimitCheckerWorker call() throws Exception {
		ARSearch.featuriseSet_calledByWorker(
				trainingFlag,
//				nnWeightings,
				targetPrograms, 
				responses,  indices, responseLog);
		return this;
	}
}
