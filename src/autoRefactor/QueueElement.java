package autoRefactor;

import java.util.Comparator;

public class QueueElement implements Comparator<QueueElement>{
	int[] position;
	double value;
	
	public QueueElement(int[] pos,double v){
		position = pos;
		value = v;
	}

	@Override
	public int compare(QueueElement o1, QueueElement o2) {
		return Double.compare(o2.value, o1.value);
	}
}
