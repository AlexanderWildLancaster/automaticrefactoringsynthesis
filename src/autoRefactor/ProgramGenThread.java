package autoRefactor;

import java.util.ArrayList;

public class ProgramGenThread extends Thread{

	private int param_restrictionCode;
	private int nToBuild;
	ArrayList<int[][]> programs;
	public boolean buildComplete = false;

	public ProgramGenThread(int nToReturn,int restriction){
		this.param_restrictionCode = restriction;
		this.nToBuild = nToReturn;
	}
	
	@Override
	public void run() {
		while(programs == null){
			try{
				int sampleSize = nToBuild/10;
				this.programs = ARExploreByMutation.generatefromEmpty(param_restrictionCode,sampleSize,sampleSize);
			}catch(Exception e){
				e.printStackTrace();
				System.out.println("Exception thrown, caught, repeating cycle");
			}
		}
		int cycles = 0;
		while(cycles < 1) {
			try{
				int sampleSize = nToBuild/5;
				programs = ARExploreByMutation.generateFromSet(param_restrictionCode,sampleSize*2,sampleSize,programs);
				cycles += 1;
			}catch(Exception e){
				e.printStackTrace();
				System.out.println("Exception thrown, caught, repeating cycle");
			}
		}
		while(true){
			try{
				programs = ARExploreByMutation.generateFromSet(
						param_restrictionCode,
						nToBuild,
						nToBuild,
						programs);
				buildComplete = true;
				return;
			}catch(Exception e){
				e.printStackTrace();
				System.out.println("Exception thrown, caught, repeating cycle");
			}
		}
	}

}
