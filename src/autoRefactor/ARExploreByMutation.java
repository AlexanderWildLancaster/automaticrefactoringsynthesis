package autoRefactor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import main.MainClass;
import main.DistributionAnalysis;
import main.Utils;
import main.Gridlang;
import main.CorpusGenerator;
import main.LogBox;
import main.Monitor;

import problems.PEasy_AddOne;
import problems.Problem;

public class ARExploreByMutation {
	public static boolean param_useRestriction = true;
	public static boolean param_useRestriction_arrayAccess = true;
	public static int param_nRestrictions = 5;

//	public static int ARSearch.nVars = -1;
//	public static int ARSearch.nArrays = -1;
//	public static int ARSearch.nLines = -1;
	public static int nExamples = 10;
	public static int maxMutationDistance = 10;
	private static int nRepeatsPerProgram = 25;
	public static Gridlang lang = new Gridlang();
	public static ArrayList<int[]> opts;
	private static int param_maxAcceptableOutputScale = 64;
	private static int param_nSplits = 3;
//	private static boolean param_useFullCorpusIteration = true;
	public static boolean param_useIteratedSeedPrograms;
	public static boolean param_useAlteredSeedPrograms = false;
	public static boolean param_useHeuristicCorpusGen = true;
	public static int param_nCorpusRebuilds = 2;
	//	private static int restrictionCode;
	private static final int RES_NONE = 0;
	private static final int RES_LOOP = 1;
	private static final int RES_LOOP_AND_COND = 2;
	private static final int RES_LOOP_AND_EQCOND = 3;
	private static final int RES_LOOP_AND_ELSECOND = 4;



	public static ArrayList<int[][]> generateAndReturnTrainingSet(int restrictionCode,int nToSupersample,int nToReturn){
		opts = lang.getMaximalOptions(ARSearch.nVars, ARSearch.nArrays);
		System.out.println("OPTS SIZE: " + opts.size());
		ArrayList<int[][]> programs = generatefromEmpty(restrictionCode,250,250);

		int cycles = 0;
		while(cycles < param_nCorpusRebuilds ) {
			try{
				programs = generateFromSet(restrictionCode,1000,500,programs,new ArrayList<int[][]>());
				cycles += 1;
			}catch(Exception e){
				e.printStackTrace();
				System.out.println("Exception thrown, caught, repeating cycle");
			}
		}
		while(true){
			try{
				ArrayList<int[][]> bulkPrograms = generateFromSet(restrictionCode,nToSupersample,nToReturn,programs,new ArrayList<int[][]>());
				return bulkPrograms;
			}catch(Exception e){
				e.printStackTrace();
				System.out.println("Exception thrown, caught, repeating cycle");
			}
		}
	}

	public static ArrayList<int[][]> generateAndReturnTrainingSet_catStrat(int nToSupersample,int nToReturn){
		opts = lang.getMaximalOptions(ARSearch.nVars, ARSearch.nArrays);
		System.out.println("OPTS SIZE: " + opts.size());

		ArrayList<int[][]> responses = new ArrayList<int[][]>();

		ArrayList<ProgramGenThread> threads = new ArrayList<ProgramGenThread>();
		for (int s=0;s<param_nSplits;s++){
			for (int r=0;r<param_nRestrictions;r++){
				int nToBuild = nToSupersample/param_nRestrictions;
				nToBuild /= param_nSplits ;
				nToBuild += 100;
				int restrictionCode = r;

				ProgramGenThread thread = new ProgramGenThread(nToBuild,restrictionCode);
				thread.start();
				threads.add(thread);
				System.out.println("Starting new thread. Now have " + threads.size());
			}
		}

		long startTime = System.currentTimeMillis();
		waitLoop:
			while(true){
				int nDone = 0;
				for (ProgramGenThread t:threads){
					if (t.buildComplete){nDone += 1;}
				}
				if (nDone == threads.size()){
					break waitLoop;
				}else{
					try {
						long t = System.currentTimeMillis() - startTime;
						t /= 1000;
						System.out.println("Waiting for threads (" + nDone + "/" + threads.size() + " finished). Wait time " + Utils.commas(t) + "s");
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		
		System.out.println("All threads complete");
		for (ProgramGenThread t:threads){
			responses.addAll(t.programs);
		}


		//Remove all spares
		Collections.shuffle(responses);
		while(responses.size() > nToReturn){
			responses.remove(responses.size()-1);
		}
		return responses;
	}
	//	public static void generateAndWriteToDiskTrainingSet(){
	//		ArrayList<int[][]> programs = generatefromEmpty(500,100);
	//
	//		for (int i=0;i<10;i++) {
	//			programs = generateFromSet(1000,500,programs);
	//		}
	//		ArrayList<int[][]> bulkPrograms = generateFromSet(200,100,programs);
	//		ArrayList<int[][]> trainPrograms = new ArrayList<int[][]>();
	//		ArrayList<int[][]> testPrograms = new ArrayList<int[][]>();
	//		int splitLine = bulkPrograms.size()/2;
	//		trainPrograms.addAll(bulkPrograms.subList(0, splitLine));
	//		testPrograms.addAll(bulkPrograms.subList(splitLine, bulkPrograms.size()));
	//
	//
	//		LogBox features_training = new LogBox("attdata_training_features");
	//		LogBox labels_training = new LogBox("attdata_training_labels");
	//		writeToDisk(trainPrograms,features_training,labels_training);
	//		LogBox features_testing = new LogBox("attdata_testing_features");
	//		LogBox labels_testing = new LogBox("attdata_testing_labels");
	//		writeToDisk(testPrograms,features_testing,labels_testing);
	//	}

	public static void writeToDisk(ArrayList<int[][]> programs,LogBox features,LogBox labels) {
		Problem p = new PEasy_AddOne();
		Gridlang lang = new Gridlang();
		ArrayList<int[]> lineOpts = lang.getMaximalOptions(ARSearch.nVars, ARSearch.nArrays);

		for (int q=0;q<nRepeatsPerProgram ;q++) {
			for (int[][] program:programs) {
				if (q == 0){
					System.out.println("----");
					System.out.println("" + score(program));
					System.out.println("----");
				}
				for (int e=0;e<nExamples;e++){
					String feature = "";
					int nLength = 8;
					int param = Utils.random.nextInt(4) + 1;
					int[] input = p.randomInput(nLength);
					input[0] = param;

					int[] output = lang.process(program, ARSearch.nArrays, ARSearch.nVars, input, param);

					for (int i=0;i<input.length;i++){
						feature += input[i] + " ";
					}
					for (int i=0;i<output.length;i++){
						feature += output[i] + " ";
					}
					feature = feature.substring(0,feature.length()-1);
					features.takeLine(feature);

					if (q == 0){
						System.out.print("I: ");
						for (int i:input){
							System.out.print(i + " ");
						}
						System.out.println();
						System.out.print("O: ");
						for (int i:output){
							System.out.print(i + " ");
						}
						System.out.println();
						System.out.println();
					}

				}
				features.takeLine("end");

				String lineLabels = "";
				for (int i=0;i<program.length;i++) {
					int[] line = program[i];
					for (int j=0;j<lineOpts.size();j++) {
						if (arraysMatch(lineOpts.get(j),line)) {
							lineLabels += j + " ";
							break;
						}
					}
				}

				labels.takeLine(lineLabels);
			}
		}
	}

	public static double getSortScore() {
		ArrayList<int[]> sortList = MainClass.getProgramSort();
		int[][] sort = new int[sortList.size()][];
		for (int i=0;i<sort.length;i++) {
			sort[i] = sortList.get(i);
		}

		return score(sort);
	}
	public static boolean arraysMatch(int[] a,int[] b) {
		for (int i=0;i<a.length;i++) {
			if (a[i] != b[i]) {return false;}
		}
		return true;
	}

	public static ArrayList<int[][]> generatefromEmpty(int restrictionCode,int nToGenerate,int nToReturn) {
		ArrayList<int[][]> programs = null;
		if (param_useIteratedSeedPrograms){
			programs = getIteratedSeedPrograms(restrictionCode);
		}else{
			if (param_useAlteredSeedPrograms){
				programs = getAlteredSeedPrograms(restrictionCode);
			}else{
				programs = getBasicSeedPrograms(restrictionCode);
			}
		}
		
		return generateFromSet(restrictionCode,nToGenerate,nToReturn,programs);
	}

	private static ArrayList<int[][]> getIteratedSeedPrograms(int restrictionCode) {
		ArrayList<int[][]> programs = getBasicSeedPrograms(restrictionCode);
		
		ArrayList<int[][]> reseeds = ARGenerator.loadReseedPrograms();
		int added = 0;
		for (int[][] program:reseeds){
			program = CorpusGenerator.ablate_noReshuffle(program, new PEasy_AddOne(),
					ARSearch.nArrays, ARSearch.nVars);
			
			if (checkRestriction(restrictionCode,program)){
				if (!checkStability(program)){
					System.out.println("Bad seed program.");
					continue;
				}
				boolean alreadyPresent = setContainsFunctionallyIdenticalProgram(programs,program);
				if (!alreadyPresent){
					programs.add(program);
					added += 1;
				}
			}
		}
		System.out.println("Loaded " + reseeds.size() + " reseeds for code " + restrictionCode + " added " + added);
		return programs;
	}

	private static ArrayList<int[][]> getAlteredSeedPrograms(int restrictionCode) {
		ArrayList<int[]> lines = new ArrayList<int[]>();
		if (restrictionCode == -1) {
			
		}
		else if (restrictionCode ==  RES_NONE) {
			//Nothing, start with empty program
		}
		else if (restrictionCode == RES_LOOP) {
			lines.add(new int[] {Gridlang.OP_LOOP,2,0,0,0});//iterator to 2
			lines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,2,3,0});//var_3 = input[var_2]
			lines.add(new int[] {Gridlang.OP_VAR_TO_ARRAY_ASSIGN,1,2,3,0});//output[var_3] = var_3
			lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endloop
		}
		else if (restrictionCode == RES_LOOP_AND_COND) {
//			lines.add(new int[] {Gridlang.OP_LOOP,2,0,0,0});//iterator to 2
//			lines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,2,3,0});//var_3 = input[var_2]
//			lines.add(new int[] {Gridlang.OP_COND,3,0,0,0});
//			lines.add(new int[] {Gridlang.OP_VAR_TO_ARRAY_ASSIGN,1,2,3,0});//output[var_3] = var_3
//			lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endif
//			lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endloop
			
			lines.add(new int[] {Gridlang.OP_LOOP,2,0,0,0});//iterator to 2
			lines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,2,3,0});//var_3 = input[var_2]
			lines.add(new int[] {Gridlang.OP_VAR_TO_ARRAY_ASSIGN,1,2,3,0});//output[var_3] = var_3
			lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endloop
		}
		else if (restrictionCode == RES_LOOP_AND_EQCOND) {
//			lines.add(new int[] {Gridlang.OP_LOOP,2,0,0,0});//iterator to 2
//			lines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,2,3,0});//var_3 = input[var_2]
//			lines.add(new int[] {Gridlang.OP_COND_EQUALS,3,1,0,0});
//			lines.add(new int[] {Gridlang.OP_VAR_TO_ARRAY_ASSIGN,1,2,3,0});//output[var_3] = var_3
//			lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endif
//			lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endloop
			
			lines.add(new int[] {Gridlang.OP_LOOP,2,0,0,0});//iterator to 2
			lines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,2,3,0});//var_3 = input[var_2]
			lines.add(new int[] {Gridlang.OP_VAR_TO_ARRAY_ASSIGN,1,2,3,0});//output[var_3] = var_3
			lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endloop
		}
		else if (restrictionCode == RES_LOOP_AND_ELSECOND) {
//			lines.add(new int[] {Gridlang.OP_LOOP,2,0,0,0});//iterator to 2
//			lines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,2,3,0});//var_3 = input[var_2]
//			lines.add(new int[] {Gridlang.OP_COND,3,0,0,0});
//			lines.add(new int[] {Gridlang.OP_VAR_TO_ARRAY_ASSIGN,1,2,3,0});//output[var_3] = var_3
//			lines.add(new int[] {Gridlang.OP_ELSEBLOCK,0,0,0,0});
//			lines.add(new int[] {Gridlang.OP_ADD,4,3,3,0});//var_4 = var_3 + var+3
//			lines.add(new int[] {Gridlang.OP_VAR_TO_ARRAY_ASSIGN,1,2,4,0});//output[var_2] = var_4
//			lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endif
//			lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endloop
			
			lines.add(new int[] {Gridlang.OP_LOOP,2,0,0,0});//iterator to 2
			lines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,2,3,0});//var_3 = input[var_2]
			lines.add(new int[] {Gridlang.OP_VAR_TO_ARRAY_ASSIGN,1,2,3,0});//output[var_3] = var_3
			lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endloop
		}
		else {
			throw new Error("Unhandled restriction code: " + restrictionCode);
		}


		int[][] seedProgram = new int[ARSearch.nLines][5];
		for (int i=0;i<lines.size();i++) {
			seedProgram[i] = lines.get(i);
		}

		ArrayList<int[][]> programs = new ArrayList<int[][]>();
		programs.add(seedProgram);
		return programs;
	}
	
	private static ArrayList<int[][]> getBasicSeedPrograms(int restrictionCode) {
		ArrayList<int[]> lines = new ArrayList<int[]>(); 
		if (restrictionCode ==  -1) {
			//Nothing, start with empty program
		}
		else if (restrictionCode ==  RES_NONE) {
			//Nothing, start with empty program
		}
		else if (restrictionCode == RES_LOOP) {
			lines.add(new int[] {Gridlang.OP_LOOP,2,0,0,0});//iterator to 2
			lines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,2,3,0});//var_3 = input[var_2]
			lines.add(new int[] {Gridlang.OP_VAR_TO_ARRAY_ASSIGN,1,2,3,0});//output[var_3] = var_3
			lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endloop
		}
		else if (restrictionCode == RES_LOOP_AND_COND) {
			lines.add(new int[] {Gridlang.OP_LOOP,2,0,0,0});//iterator to 2
			lines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,2,3,0});//var_3 = input[var_2]
			lines.add(new int[] {Gridlang.OP_COND,3,0,0,0});
			lines.add(new int[] {Gridlang.OP_VAR_TO_ARRAY_ASSIGN,1,2,3,0});//output[var_3] = var_3
			lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endif
			lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endloop
		}
		else if (restrictionCode == RES_LOOP_AND_EQCOND) {
			lines.add(new int[] {Gridlang.OP_LOOP,2,0,0,0});//iterator to 2
			lines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,2,3,0});//var_3 = input[var_2]
			lines.add(new int[] {Gridlang.OP_COND_EQUALS,3,1,0,0});
			lines.add(new int[] {Gridlang.OP_VAR_TO_ARRAY_ASSIGN,1,2,3,0});//output[var_3] = var_3
			lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endif
			lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endloop
		}
		else if (restrictionCode == RES_LOOP_AND_ELSECOND) {
			lines.add(new int[] {Gridlang.OP_LOOP,2,0,0,0});//iterator to 2
			lines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,2,3,0});//var_3 = input[var_2]
			lines.add(new int[] {Gridlang.OP_COND,3,0,0,0});
			lines.add(new int[] {Gridlang.OP_VAR_TO_ARRAY_ASSIGN,1,2,3,0});//output[var_3] = var_3
			lines.add(new int[] {Gridlang.OP_ELSEBLOCK,0,0,0,0});
			lines.add(new int[] {Gridlang.OP_ADD,4,3,3,0});//var_4 = var_3 + var+3
			lines.add(new int[] {Gridlang.OP_VAR_TO_ARRAY_ASSIGN,1,2,4,0});//output[var_2] = var_4
			lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endif
			lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endloop
		}
		else {
			throw new Error("Unhandled restriction code: " + restrictionCode);
		}


		int[][] seedProgram = new int[ARSearch.nLines][5];
		for (int i=0;i<lines.size();i++) {
			seedProgram[i] = lines.get(i);
		}

		ArrayList<int[][]> programs = new ArrayList<int[][]>();
		programs.add(seedProgram);
		return programs;
	}

	public static ArrayList<int[][]> generateFromSet(int restrictionCode,int nToGenerate,int nToReturn,ArrayList<int[][]> programs){
		return generateFromSet(restrictionCode,nToGenerate,nToReturn,programs,new ArrayList<int[][]>());
	}

	public static ArrayList<int[][]> generateFromSet(int restrictionCode,int nToGenerate,int nToReturn,ArrayList<int[][]> originalPrograms,ArrayList<int[][]> exclude){
		ArrayList<Double> scores = new ArrayList<Double>();
		double bestScore = -1;
		int[][] bestProgram = null;

		if (nToReturn > nToGenerate) {
			nToGenerate = nToReturn;
		}

		System.out.println("Generating from set. Starting with " + originalPrograms.size() + " original programs...");

		ArrayList<int[][]> programs = new ArrayList<int[][]>();
		programs.addAll(originalPrograms);
		for (int[][] program:programs) {
			double localScore = score(program);
			scores.add(localScore);
			if (localScore > bestScore) {
				bestScore = localScore;
				bestProgram = program;
			}
		}

		//				double sortScore = getSortScore();
		double sortScore = -1;
		while(programs.size() < nToGenerate){
			double[] fitnesses = new double[scores.size()];
			for (int i=0;i<scores.size();i++) {
				fitnesses[i] = scores.get(i);
			}
			int indexChosen = Utils.roulette(fitnesses);
			int[][] parent = clone(programs.get(indexChosen));
			//			int[][] parent = clone(programs.get(Eleven.random.nextInt(programs.size())));
			int[][] mutant = mutate(parent);

			if (!checkStability(mutant)){continue;}
			try{
				if (setContainsFunctionallyIdenticalProgram(programs,mutant)){continue;}
				if (setContainsFunctionallyIdenticalProgram(exclude,mutant)){continue;}
				if (!checkRespectForOutputScales(mutant)){continue;}
			}catch(Exception e){continue;}//Skip if it proved later to be unstable
			int[][] ablated = mutant;
//			if (!param_useIteratedSeedPrograms){
				ablated = CorpusGenerator.ablateFast(mutant, new PEasy_AddOne(), ARSearch.nArrays, ARSearch.nVars);
				if (ablated == null) {continue;}
//			}
			if (param_useRestriction && (!checkRestriction(restrictionCode,ablated))){continue;}
			if (param_useRestriction_arrayAccess  && (!checkArrayAccess(ablated))){continue;}
			if (!checkStability(ablated)){System.out.println("Ablation introduced bugs");continue;}

			//We now know it's functionally dissimilar from any existing program
			programs.add(ablated);
			int nLines = 0;
			for (int i=0;i<ablated.length;i++){
				if (ablated[i][0] != 0){
					nLines += 1;
				}
			}
			double score = score(ablated);
			scores.add(score);
			System.out.println("Mutant found. " + programs.size() + "/" + nToGenerate 
					+ " Score: " + ((int)score) + " ARSearch.nLines: " + nLines + " bestEver " 
					+ ((int)bestScore) + " SortScore: " + ((int)sortScore));

			if (score > bestScore) {
				bestScore = score;
				bestProgram = ablated;
			}
		}

		Problem p = new PEasy_AddOne();
		for (int example=0;example<10;example++) {
			int[] input = p.randomInput(8);
			int param = Utils.random.nextInt(4) + 1;
			int[] output = lang.process(bestProgram, ARSearch.nArrays, ARSearch.nVars, input, param);

			for(int i:input) {
				System.out.print(i + " ");
			}
			System.out.println(" | " + param);
			for (int i:output) {
				System.out.print(i + " ");
			}
			System.out.println();
		}

		System.out.println("----Program-----");
		for (int line=0;line<bestProgram.length;line++) {
			for (int i:bestProgram[line]) {
				System.out.print(i + " ");
			}
			System.out.println();
		}

//		if (!param_useIteratedSeedPrograms ){
			for (int i=0;i<programs.size();i++){
				int[][] refactored = CorpusGenerator.refactorVariableNaming(programs.get(i), p, ARSearch.nArrays, ARSearch.nVars);
	
				if (refactored == null){
					System.out.println("Refactoring returned null");
				}
				if (!CorpusGenerator.checkStability(refactored, ARSearch.nVars, ARSearch.nArrays, lang)){
					System.out.println(i + " Stability not guaranteed");
					boolean prevStability = CorpusGenerator.checkStability(programs.get(i), ARSearch.nVars, ARSearch.nArrays, lang);
					if (prevStability){
						throw new Error("Refactoring appears to have introduced a bug");
					}
				}
				programs.set(i, refactored);
			}
//		}

		long time = System.currentTimeMillis();
		System.out.println("Reducing to diverent set. From " + programs.size() + " to " + nToReturn + "...");
		ArrayList<int[][]> reply = reduceToDivergentSet(programs,nToReturn);
		time = System.currentTimeMillis() - time;
		System.out.println("Reduced. Time " + Utils.commas(time) + "ms");
		return reply;
	}

	private static boolean checkArrayAccess(int[][] program){
		int nRuns = 32;

		try{

			boolean hasLoop = false;
			for (int[] line:program){
				if (line[0] == Gridlang.OP_LOOP){
					hasLoop = true;
				}
			}

			Gridlang interpreter = new Gridlang();
			int maxStepsUsed = 0;
			for (int r=0;r<nRuns;r++){
				Monitor m = new Monitor(ARSearch.nLines, ARSearch.nArrays,ARSearch.nVars);
				int param = 1 + Utils.random.nextInt(7);
				int[] in = new int[8];
				for (int i=0;i<in.length;i++) {
					in[i] = Utils.random.nextInt(16) - 8;
				}
				interpreter.process(program, ARSearch.nArrays, ARSearch.nVars, in.clone(), param,m);
				maxStepsUsed = Math.max(m.stepsUsed, maxStepsUsed);
				m.stepsUsed = 0;

				//An array cell shouldn't be read or written to more than once
				int[][] counts = new int[m.arrayAccessHistories[0].length][m.arrayAccessHistories[0][0].length];
				int totalReads = 0;
				for (int t=0;t<m.arrayAccessHistories.length;t++){
					for (int arr=0;arr<m.arrayAccessHistories[0].length;arr++){
						for (int ind=0;ind<m.arrayAccessHistories[0][0].length;ind++){
							if (m.arrayAccessHistories[t][arr][ind] > 0){
								if (counts[arr][ind] != 0){
									return false;
								}

								counts[arr][ind] += 1;
								totalReads += 1;
							}
						}
					}
				}
				counts = new int[m.arrayWriteHistories[0].length][m.arrayWriteHistories[0][0].length];
				for (int t=0;t<m.arrayWriteHistories.length;t++){
					for (int arr=0;arr<m.arrayWriteHistories[0].length;arr++){
						for (int ind=0;ind<m.arrayWriteHistories[0][0].length;ind++){
							if (m.arrayWriteHistories[t][arr][ind] > 0){
								if (counts[arr][ind] != 0){
									return false;
								}
								counts[arr][ind] += 1;
							}
						}
					}
				}
				if (hasLoop){
					if (totalReads != in.length){
						return false;
					}
				}
			}
			return true;
		}catch(Exception e){
			return false;
		}
	}

	private static boolean checkRestriction(int restrictionCode,int[][] mutant) {
		if (restrictionCode == -1) {return true;}
		try{
			//Check if a control statement is dead
			for (int i=0;i<mutant.length-1;i++){
				//It is legal to put an else immediately after a cond
				//			if (mutant[i][0] == Gridlang.OP_COND && mutant[i+1][0] == Gridlang.OP_ELSEBLOCK){return false;}
				if (mutant[i][0] == Gridlang.OP_COND && mutant[i+1][0] == Gridlang.OP_ENDBLOCK){return false;}
				if (mutant[i][0] == Gridlang.OP_LOOP && mutant[i+1][0] == Gridlang.OP_ENDBLOCK){return false;}
				if (mutant[i][0] == Gridlang.OP_ELSEBLOCK && mutant[i+1][0] == Gridlang.OP_ENDBLOCK){return false;}

			}


			int condCount = 0;
			int loopCount = 0;
			int condEqCount = 0;
			int elseCount = 0;
			for (int[] line:mutant){
				if (line[0] == Gridlang.OP_COND){condCount += 1;}
				if (line[0] == Gridlang.OP_LOOP){loopCount += 1;}
				if (line[0] == Gridlang.OP_COND_EQUALS){condEqCount += 1;}
				if (line[0] == Gridlang.OP_ELSEBLOCK){elseCount += 1;}
			}

			if (restrictionCode == RES_NONE){
				if (condCount != 0){return false;}
				if (loopCount != 0){return false;}
				if (condEqCount != 0){return false;}
				if (elseCount != 0){return false;}
			}
			else if (restrictionCode == RES_LOOP){
				if (condCount != 0){return false;}
				if (loopCount != 1){return false;}
				if (condEqCount != 0){return false;}
				if (elseCount != 0){return false;}
			}
			else if (restrictionCode == RES_LOOP_AND_COND){
				if (condCount != 1){return false;}
				if (loopCount != 1){return false;}
				if (condEqCount != 0){return false;}
				if (elseCount != 0){return false;}
			}
			else if (restrictionCode == RES_LOOP_AND_EQCOND){
				if (condCount != 0){return false;}
				if (loopCount != 1){return false;}
				if (condEqCount != 1){return false;}
				if (elseCount != 0){return false;}
			}
			else if (restrictionCode == RES_LOOP_AND_ELSECOND){
				if (condCount != 1){return false;}
				if (loopCount != 1){return false;}
				if (condEqCount != 0){return false;}
				if (elseCount != 1){return false;}
			}else{
				throw new Error(restrictionCode + " resCode too high");
			}
			return true;

			//		boolean hasCond = false;
			//		boolean hasLoop = false;
			//		for (int[] line:mutant){
			//			if (line[0] == Gridlang.OP_COND){hasCond = true;}
			//			if (line[0] == Gridlang.OP_LOOP){hasLoop = true;}
			//		}
			//		
			//		return hasCond && hasLoop;
		}catch(Exception e){
			return false;
		}
	}


	public static boolean checkRespectForOutputScales(int[][] program) {
		try {
			for (int t=0;t<50;t++) {
				int[] input = new int[8];
				for (int i=0;i<input.length;i++){
					input[i] = Utils.random.nextInt(16) - 8;
				}
				int param = 1 + Utils.random.nextInt(7);
				int[] out = lang.process(program, ARSearch.nArrays, ARSearch.nVars, input.clone(), param);
				for (int i:out){
					if (Math.abs(i) > param_maxAcceptableOutputScale  ){
						return false;
					}
				}

				//				System.out.print("Range: ");
				//				for (int i:out){
				//					System.out.print(i + " ");
				//				}
				//				System.out.println();
			}
		}catch(Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * The aim is to pick out the most interesting programs, and to pick those furthest from each other
	 * We want to cover as much of the space as possible, to try to allow generalisation by the NN
	 * 
	 * Two metrics for distance exist: 	-distance in terms of output
	 * 									-distance in terms of code
	 * 
	 * Distance of code needs to take the possibility of offsetting, can't just go
	 * 		dist += |program1[i]  - program2[i]|
	 */
	public static ArrayList<int[][]> reduceToDivergentSet(ArrayList<int[][]> programs,int nToReturn){
		if (programs.size() <= nToReturn) {return programs;}

		ArrayList<int[][]> reducedSet = new ArrayList<int[][]>();
		Gridlang lang = new Gridlang();

		//Build a giant set of outputs
		int nExamples = 8;
		int[][] inputs = new int[nExamples][8];
		int[] params = new int[nExamples];
		int[][][] outputs = new int[programs.size()][nExamples][8];
		for (int i=0;i<inputs.length;i++){
			for (int j=0;j<inputs[0].length;j++){
				inputs[i][j] = Utils.random.nextInt(16) - 8;
			}
		}
		for (int i=0;i<inputs.length;i++){
			params[i] = 1 + Utils.random.nextInt(4);
		}

		for (int pr=0;pr<programs.size();pr++){
			for (int e=0;e<nExamples;e++){
				int[] localOutput = lang.process(programs.get(pr), ARSearch.nArrays, ARSearch.nVars, inputs[e], params[e]);
				outputs[pr][e] = localOutput;
			}
		}

		double bestScore = 0;
		int[][] bestProgram = programs.get(0);
		int bestIndex = 0;
		double[] scores = new double[programs.size()];
		for (int i=0;i<programs.size();i++){
			double score = score(programs.get(i));
			scores[i] = score;
			if (score > bestScore){
				bestScore = score;
				bestProgram = programs.get(i);
				bestIndex = i;
			}

			//			double distCode = getCodeDistance(programs.get(i),programs.get(programs.size()-1));
			//			double distOut = getOutputDistance(outputs,i,programs.size()-1);
			//			System.out.println(i + " " + distCode + " " + distOut);
		}

		//We don't need to keep all distances, just the closest one from the reducedSet
		//to each other program, since we're trying to maximise distance from closest
		double[] minDistances = new double[programs.size()];
		for (int i=0;i<minDistances.length;i++){
			minDistances[i] = getCodeDistance(bestProgram,programs.get(i))
					*
					getOutputDistance(outputs,bestIndex,i);
		}
		reducedSet.add(bestProgram);
		for (int t=0;t<nToReturn-1;t++){
			double best = -1;
			bestIndex = -1;
			for (int i=0;i<minDistances.length;i++){
				double productScore = scores[i]*minDistances[i];
				if (productScore > best){
					best = productScore;
					bestIndex = i;
				}
			}
			if (bestIndex == -1){return reducedSet;}

			reducedSet.add(programs.get(bestIndex));
			//For each other program, find out if this is the closest one
			for (int i=0;i<minDistances.length;i++){
				minDistances[i] = Math.min(minDistances[i],getCodeDistance(bestProgram,programs.get(i))
						*
						getOutputDistance(outputs,bestIndex,i));
			}
		}

		return reducedSet;
	}

	public static double getCodeDistance(int[][] programA,int[][] programB){
		double dist = 1;//Start at 1 or it'll multiply in and become 0 far too often

		for (int i=0;i<programA.length;i++){
			int op = programA[i][0];
			//See if the other program has the same operation somewhere nearby
			//Scan indices above, at and below this code point to see if they share an operation
			//Basic defence against off-by-one modifications confusin operation matching
			boolean matched = false;
			for (int j=-1;j<2;j++){
				if (j + i < 0){continue;}
				if (j + i >= programA.length) {continue;}
				if (programB[i+j][0] == op){
					matched = true;
					break;
				}
			}
			if (!matched){
				dist += 1;
			}
		}
		return dist;
	}

	public static double getOutputDistance(int[][][] outputs,int ind1,int ind2){
		double dist = 0;
		for (int i=0;i<outputs[0].length;i++){
			for (int j=0;j<outputs[0][0].length;j++){
				if (outputs[ind1][i][j] != outputs[ind2][i][j]){
					dist += 1;
				}
			}
		}
		return dist;
	}

	public static boolean setContainsFunctionallyIdenticalProgram(ArrayList<int[][]> existing,int[][] candidate) {
		for (int[][] program:existing) {
			if (ARSearch.areFunctionallyIdentical(program, candidate, ARSearch.nVars, ARSearch.nArrays, lang)) {
				return true;
			}
		}
		return false;
	}


	public static double score(int[][] program){
		if (param_useHeuristicCorpusGen ){
		//Of "distrib, uniform, itr defence", "hand crafted" and "constant score"
		//"distrib et al" was found to be somewhat superior (within noise, may not truly be best"
		//For corpus of 1500 programs, restriction codes for 5 types of flow control use
				return score_distributionAndUniformity_withIteratorDefence(program);
		}else{
			return 100;
		}
	}

	public static double score_distributionAndUniformity(int[][] program){
		double totalScore = 1;

		Monitor monitor = new Monitor(program.length,ARSearch.nArrays,ARSearch.nVars);
		int nRunthroughs = 32;
		Problem p = new PEasy_AddOne();
		for (int i=0;i<nRunthroughs;i++){
			int[] input = p.randomInput(8);
			int param = Utils.random.nextInt(4) + 1;
			lang.process(program, ARSearch.nArrays, ARSearch.nVars, input, param, monitor);
		}

		totalScore = DistributionAnalysis.getDistributionScore(program);

		for (int i=0;i<monitor.arrayAccessCounts.length;i++){
			if (monitor.arrayAccessCounts[i] != null){
				totalScore *= scoreMetric_equality(monitor.arrayAccessCounts[i]);
			}
		}

		return totalScore*1000;
	}

	public static double score_distributionAndUniformity_withIteratorDefence(int[][] program){
		double totalScore = 1;

		Monitor monitor = new Monitor(program.length,ARSearch.nArrays,ARSearch.nVars);
		int nRunthroughs = 32;
		Problem p = new PEasy_AddOne();
		for (int i=0;i<nRunthroughs;i++){
			int[] input = p.randomInput(8);
			int param = Utils.random.nextInt(4) + 1;
			lang.process(program, ARSearch.nArrays, ARSearch.nVars, input, param, monitor);
		}
		//		double maxSteps = Gridlang.maxSteps*nRunthroughs;


		totalScore = DistributionAnalysis.getDistributionScore(program);

		//Length score
		double lenScore = 2;
		for (int i=0;i<program.length;i++){
			if (program[i][0] != 0){
				lenScore += 1;
			}
		}
		totalScore *= lenScore;

		//		//-----ITERATOR VARIABLE DEFENCE
		//Avoid writing to the iterators themselves, that just upsets the looping behaviour
		boolean[] isIterator = new boolean[ARSearch.nVars+Gridlang.builtinVars];
		for (int i=0;i<program.length;i++) {
			if (program[i][0] == Gridlang.OP_LOOP) {
				if (program[i][1] >= isIterator.length){
					System.out.print("Line: ");
					for (int k:program[i]){
						System.out.print(k + " ");
					}
					System.out.println();
					//					throw new Error("Overran in variable assessment");
					System.out.println("Error: Overran in variable assessment");
				}
				isIterator[program[i][1]] = true;
			}
		}
		for (int i=0;i<program.length;i++) {
			if (program[i][0] > 0 && program[i][0] < 100) {//Arithmetic op, writing to PARAM_0
				if (isIterator[program[i][1]]) {
					totalScore *= 0.75;
				}
			}
		}

		for (int i=0;i<monitor.arrayAccessCounts.length;i++){
			if (monitor.arrayAccessCounts[i] != null){
				totalScore *= scoreMetric_equality(monitor.arrayAccessCounts[i]);
			}
		}

		return totalScore*1000;
	}
	public static double score_handCrafted(int[][] program){
		Problem p = new PEasy_AddOne();
		double totalScore = 0;

		//Firstly, see how much it differs under various obvious input sets
		double varietyScore = getVariationScore(program);
		totalScore += varietyScore;


		//-------RUNTIME SCORE------
		//Tries to promote use of loops
		//Just counting the loops doesn't do much, as it can have loops with very low runthroughs
		Monitor monitor = new Monitor(program.length,ARSearch.nArrays,ARSearch.nVars);
		int nRunthroughs = 32;
		for (int i=0;i<nRunthroughs;i++){
			int[] input = p.randomInput(8);
			int param = Utils.random.nextInt(4) + 1;
			lang.process(program, ARSearch.nArrays, ARSearch.nVars, input, param, monitor);
		}
		double maxSteps = Gridlang.maxSteps*nRunthroughs;
		double stepsUsed = monitor.stepsUsed;
		stepsUsed /= maxSteps;//0 to 1, with 1 being maxSteps
		double runtimeScore = 0.2 + stepsUsed;
		totalScore *= runtimeScore;

		//Length score
		double lenScore = 2;
		for (int i=0;i<program.length;i++){
			if (program[i][0] != 0){
				lenScore += 1;
			}
		}
		totalScore *= lenScore;

		//----------------
		//		
		//		//-----ITERATOR VARIABLE DEFENCE
		//Avoid writing to the iterators themselves, that just upsets the looping behaviour
		boolean[] isIterator = new boolean[ARSearch.nVars+Gridlang.builtinVars];
		for (int i=0;i<program.length;i++) {
			if (program[i][0] == Gridlang.OP_LOOP) {
				if (program[i][1] >= isIterator.length){
					System.out.print("Line: ");
					for (int k:program[i]){
						System.out.print(k + " ");
					}
					System.out.println();
					//					throw new Error("Overran in variable assessment");
					System.out.println("Error: Overran in variable assessment");
				}
				isIterator[program[i][1]] = true;
			}
		}
		for (int i=0;i<program.length;i++) {
			if (program[i][0] > 0 && program[i][0] < 100) {//Arithmetic op, writing to PARAM_0
				if (isIterator[program[i][1]]) {
					totalScore *= 0.75;
				}
			}
		}


		double score = 0.5;
		for (int i=0;i<monitor.arrayAccessCounts.length;i++){
			if (monitor.arrayAccessCounts[i] != null){
				score += scoreMetric_inequality(monitor.arrayAccessCounts[i]);
			}
		}
		for (int i=0;i<monitor.arrayWriteCounts.length;i++){
			if (monitor.arrayWriteCounts[i] != null){
				score += scoreMetric_inequality(monitor.arrayWriteCounts[i]);
			}
		}
		totalScore *= score;

		/**
		//------ITERATOR VARIABLE IN ARRAY ACCESS----
		//We want the arrays to be accessed by variables which are functions of the iterator
		boolean[] influencedByIterator = new boolean[ARSearch.nVars+Gridlang.builtiARSearch.nArrays];
		for (int i=0;i<influencedByIterator.length;i++) {
			if (isIterator[i]) {influencedByIterator[i] = true;}
		}
		for (int line=0;line<program.length;line++) {
			if (program[line][0] > 0 && program[line][0] < 100
					&& program[line][0] != Gridlang.OP_VAR_TO_LITERAL) {//Arithmetic op
				//Write to PARAM_A, read from PARAM_B,PARM_C
				boolean usesIterator = influencedByIterator[program[line][Gridlang.PARAM_B]]
						|| influencedByIterator[program[line][Gridlang.PARAM_C]];
				influencedByIterator[program[line][Gridlang.PARAM_A]] = usesIterator;
			}
		}
		for (int line=0;line<program.length;line++) {
			//Array access
			if (program[line][0] > 100 && program[line][0] < 200) {
				//If true, our accessor variable is influenced by the iterator in some way
				//Array is param_A, variable is param_B
				//Essentially "param_a[param_b] = ..."
				if (influencedByIterator[program[line][Gridlang.PARAM_B]]) {
					totalScore *= 1.5;
				}
				//Special case, as this operation has two array accesses
				if (program[line][0] == Gridlang.OP_ARRAY_TO_ARRAY_ASSIGN) {
					if (influencedByIterator[program[line][Gridlang.PARAM_D]]) {
						totalScore *= 1.5;
					}
				}
			}
		}
		 */

		return Math.pow(totalScore,1.5);
	}

	private static double scoreMetric_equality(int[] is) {
		if (is.length < 2){return 1;}//0 or 1 length arrays are automatically perfectly uniform

		double max = 0;
		for (int i=0;i<is.length;i++){
			if (Math.abs(is[i]) > max){
				max = Math.abs(is[i]);
			}
		}

		if (max == 0){return 1;}//No values in this array, no writes were performed

		double stanDev = 0;
		for (int i=0;i<is.length;i++){
			double e = is[i]-max;
			stanDev += e*e;
		}
		stanDev /= (is.length - 1);
		double var = Math.sqrt(stanDev);

		//Max possible standard deviation is absMax 
		var /= max;//Between 1.0 (max inequality) to 0.0 (perfect equality)

		//Just assert that property
		if (var > 1){throw new Error();}
		if (var < 0){throw new Error();}

		return ((1 - var)*0.9) + 0.1;
	}

	private static double scoreMetric_inequality(int[] is) {
		if (is.length < 2){return 0;}//0 or 1 length arrays are automatically perfectly uniform

		double max = 0;
		for (int i=0;i<is.length;i++){
			if (Math.abs(is[i]) > max){
				max = Math.abs(is[i]);
			}
		}

		if (max == 0){return 0;}//No values in this array, no writes were performed

		double stanDev = 0;
		for (int i=0;i<is.length;i++){
			double e = is[i]-max;
			stanDev += e*e;
		}
		stanDev /= (is.length - 1);
		double var = Math.sqrt(stanDev);

		//Max possible standard deviation is absMax 
		var /= max;//Between 1.0 (max inequality) to 0.0 (perfect equality)

		//Just assert that property
		if (var > 1){throw new Error();}
		if (var < 0){throw new Error();}

		return ((1 - var)*0.9) + 0.25;
	}


	public static double score_old(int[][] program){
		Problem p = new PEasy_AddOne();
		double totalScore = 0;

		//Firstly, see how much it differs under various obvious input sets
		double varietyScore = getVariationScore(program);
		totalScore += varietyScore;


		//-------LENGTH SCORE-------
		//See how long it is, as a proportion of how long it could be
		//Amortised by a bit, to avoid this being too dominant a factor
		int nLinesUsed = 0;
		for (int i=0;i<program.length;i++) {
			if (program[i][0] != Gridlang.OP_NOOP) {
				nLinesUsed += 1;
			}
		}
		double maxLines = program.length;
		double lengthScore = nLinesUsed/maxLines;//0 to 1, 1 being good
		lengthScore += 0.5;//0.5 to 1.5, 1.5 being good
		totalScore *= lengthScore;
		//-----------------------


		//--------------CONDITIONAL BALANCE--------------------
		//Build conditional results for conditional balance analysis
		//Rarely relevant conditions are bad, as they could indicate conditional spam
		//This means conditions which are far from 50% pass rate
		Monitor monitor = new Monitor(program.length,ARSearch.nArrays,ARSearch.nVars);
		int nRunthroughs = 32;
		for (int i=0;i<nRunthroughs;i++){
			int[] input = p.randomInput(8);
			int param = Utils.random.nextInt(4) + 1;
			lang.process(program, ARSearch.nArrays, ARSearch.nVars, input, param, monitor);
		}
		for (int line=0;line<program.length;line++) {
			if (monitor.conditionAttempts[line] > 0) {
				double pSucceed = monitor.conditionSuccesses[line]/monitor.conditionAttempts[line];
				System.out.println("Line: " + line + " pSucceed: " + pSucceed);
				double imbalance = Math.abs(0.5 - pSucceed);//0.5 to 0, 0 desirable
				double balance = 1 - imbalance;//0.5 to 1, 1 desirable
				totalScore *= balance;
			}
		}
		//----------------------------------

		//-------CONDITIONAL AVERSION-------
		for (int i=0;i<program.length;i++) {
			if (program[i][0] == Gridlang.OP_COND) {
				totalScore *= 0.95;
			}
		}

		//-------RUNTIME SCORE------
		//Tries to promote use of loops
		//Just counting the loops doesn't do much, as it can have loops with very low runthroughs
		double maxSteps = Gridlang.maxSteps*nRunthroughs;
		double stepsUsed = monitor.stepsUsed;
		stepsUsed /= maxSteps;//0 to 1, with 1 being maxSteps
		double runtimeScore = 0.2 + stepsUsed;
		totalScore *= runtimeScore;
		//----------------

		return totalScore;
	}

	private static double getVariationScore(int[][] program) {
		ArrayList<int[]> exampleOutputs = new ArrayList<int[]>();
		int[] exampleInput;
		int exampleParam = 2;

		//0,0,0,0,0
		exampleInput = new int[8];
		exampleParam = 2;
		exampleOutputs.add(lang.process(program, ARSearch.nArrays, ARSearch.nVars, exampleInput, exampleParam));
		//		exampleOutputs.add(exampleInput.clone());

		//0,1,2,3,4,5,6,7,8
		exampleParam = 2;
		for (int i=0;i<exampleInput.length;i++) {
			exampleInput[i] = i;
		}
		exampleOutputs.add(lang.process(program, ARSearch.nArrays, ARSearch.nVars, exampleInput, exampleParam));
		//		exampleOutputs.add(exampleInput.clone());

		//0,1,-2,3,-4,5,-6,7,-8
		exampleParam = 3;
		for (int i=0;i<exampleInput.length;i++) {
			exampleInput[i] = i;
			if (i % 2 == 0) {exampleInput[i] = -i;}
		}
		exampleOutputs.add(lang.process(program, ARSearch.nArrays, ARSearch.nVars, exampleInput, exampleParam));
		//		exampleOutputs.add(exampleInput.clone());

		//2,2,2,2,2,2,2,2
		exampleParam = 4;
		for (int i=0;i<exampleInput.length;i++) {
			exampleInput[i] = 2;
		}
		exampleOutputs.add(lang.process(program, ARSearch.nArrays, ARSearch.nVars, exampleInput, exampleParam));
		//		exampleOutputs.add(exampleInput.clone());

		//0,1,0,1,0,1,0,1
		exampleParam = 2;
		for (int i=0;i<exampleInput.length;i++) {
			if (i % 2 == 1) {exampleInput[i] = 1;}
		}
		exampleOutputs.add(lang.process(program, ARSearch.nArrays, ARSearch.nVars, exampleInput, exampleParam));
		//		exampleOutputs.add(exampleInput.clone());

		//-8,4,2,-3,2,1,0,5 (human-gen-random)
		exampleParam = 3;
		exampleInput = new int[] {-8,4,2,-3,2,1,0,5};
		exampleOutputs.add(lang.process(program, ARSearch.nArrays, ARSearch.nVars, exampleInput, exampleParam));
		//		exampleOutputs.add(exampleInput.clone());


		double score = 0;
		HashSet<Integer> variety = new HashSet<Integer>();
		for (int i=0;i<exampleOutputs.get(0).length;i++) {
			variety.clear();
			for (int j=0;j<exampleOutputs.size();j++) {
				variety.add(exampleOutputs.get(j)[i]);
			}
			score += variety.size();
		}

		return score;
	}

	public static int[][] mutate(int[][] parent) {
		int[][] mutant = clone(parent);
		intermixNoOps(mutant);

		int mutationDistance = 1 + Utils.random.nextInt(maxMutationDistance);
		for (int m=0;m<mutationDistance;m++) {
			int q = Utils.random.nextInt(3);
			if (q == 0){
				mutant = insertMutate(mutant);
			}
			else if (q == 1){
				mutant = pointMutate(mutant);
			}
			//			else if (q == 2){
			//				mutant = flowMutate(mutant);
			//			}
			else{
				mutant = lineMutate(mutant);
			}
		}

		int blockCount = 0;
		int endBlockCount = 0;
		for (int i=0;i<mutant.length;i++) {
			if (mutant[i][0] == Gridlang.OP_COND) {blockCount += 1;}
			if (mutant[i][0] == Gridlang.OP_WHILE) {blockCount += 1;}
			if (mutant[i][0] == Gridlang.OP_LOOP) {blockCount += 1;}

			if (mutant[i][0] == Gridlang.OP_ENDBLOCK) {endBlockCount += 1;}
		}
		//Fix any errant parameters inserted into endblocks
		for (int i=0;i<mutant.length;i++) {
			if (mutant[i][0] == Gridlang.OP_ENDBLOCK){mutant[i] = new int[]{Gridlang.OP_ENDBLOCK,0,0,0,0};}
			if (mutant[i][0] == Gridlang.OP_ELSEBLOCK){mutant[i] = new int[]{Gridlang.OP_ELSEBLOCK,0,0,0,0};}

		}

		int missing = blockCount - endBlockCount;
		for (int i=0;i<missing;i++) {
			mutant = insertMutate_BlockEnd(mutant);
		}

		return mutant;
	}

	public static void intermixNoOps(int[][] program){
		for (int i=0;i<program.length;i++) {
			for (int j=0;j<program.length-1;j++) {
				if (program[j+1][0] == Gridlang.OP_NOOP && Utils.random.nextBoolean()) {
					int[] held = program[j+1];
					program[j+1] = program[j];
					program[j] = held;
				}
			}
		}
	}

	public static int[][] insertMutate_BlockEnd(int[][] program){
		int line = Utils.random.nextInt(program.length/2) + program.length/2;

		int[][] mutant = CorpusGenerator.clone(program);

		//Move everything below the line down one
		//Pushes one thing off the bottom
		for (int i=mutant.length-1;i>line;i--){
			mutant[i] = mutant[i-1].clone();
		}

		mutant[line] = new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0};
		return mutant;
	}

	public static int[][] flowMutate(int[][] mutant){
		int line = Utils.random.nextInt(mutant.length);

		//Move everything below the line down one
		//Pushes one thing off the bottom
		for (int i=mutant.length-1;i>line;i--){
			mutant[i] = mutant[i-1].clone();
		}

		//Find something to replace it with
		int c = 0;
		int[] replaceChoice = null;
		for (int i=0;i<opts.size();i++){
			if (opts.get(i)[0] < 0 && opts.get(i)[0] != -4){
				c += 1;
				if (Utils.random.nextInt(c) == 0){
					replaceChoice = opts.get(i);
				}
			}
		}
		mutant[line] = replaceChoice;

		//If you're at the end, stop now
		if (line > mutant.length - 2){return mutant;}

		//Otherwise, add an end-block
		int spaceRemaining = mutant.length - line - 1;
		//		System.out.println("Line " + line + " len " + mutant.length + " " + spaceRemaining);
		line += Utils.random.nextInt(spaceRemaining) + 1;

		//Move everything below the line down one
		//Pushes one thing off the bottom
		for (int i=mutant.length-1;i>line;i--){
			mutant[i] = mutant[i-1].clone();
		}

		//Loops always end
		if (replaceChoice[0] == -2){
			mutant[line] = new int[]{Gridlang.OP_ENDBLOCK,0,0,0,0};
		}
		else if (Utils.random.nextBoolean()){//Ifs sometimes end
			mutant[line] = new int[]{Gridlang.OP_ENDBLOCK,0,0,0,0};
		}else{
			//Or sometimes have an else block
			mutant[line] = new int[]{Gridlang.OP_ELSEBLOCK,0,0,0,0};
		}
		return mutant;
	}
	public static int[][] insertMutate(int[][] mutant){
		int line = Utils.random.nextInt(mutant.length);

		//Move everything below the line down one
		//Pushes one thing off the bottom
		for (int i=mutant.length-1;i>line;i--){
			mutant[i] = mutant[i-1].clone();
		}

		mutant[line] = opts.get(Utils.random.nextInt(opts.size()));
		return mutant;
	}
	public static int[][] shuffleNoOpsToBottom(int[][] program){
		//Shuffle all no-ops to the bottom
		for (int i=0;i<program.length;i++) {
			for (int j=1;j<program.length;j++) {
				if (program[j-1][Gridlang.OP] == Gridlang.OP_NOOP) {
					int[] held = program[j-1];
					program[j-1] = program[j];
					program[j] = held;
				}
			}
		}
		return program;
	}
	public static int[][] lineMutate(int[][] mutant) {
		int lineIndex = Utils.random.nextInt(mutant.length);
		int[] replacement = opts.get(Utils.random.nextInt(opts.size()));
		mutant[lineIndex] = replacement;
		return mutant;
	}
	public static int[][] pointMutate(int[][] mutant) {
		int lineIndex = Utils.random.nextInt(mutant.length);
		int[] line = mutant[lineIndex];

		int c = 0;
		int[] replacement = line;
		for (int[] opt:opts) {
			int divergences = 0;
			for (int i=0;i<opt.length;i++) {
				if (opt[i] != line[i]) {divergences += 1;}
			}
			//Only one item may be altered, all the rest must be identical
			if (divergences == 1) {
				c += 1;
				if (Utils.random.nextInt(c) == 0) {
					replacement = opt;
				}
			}
		}

		mutant[lineIndex] = replacement;
		return mutant;
	}

	public static boolean checkStability(int[][] program) {
		Problem p = new PEasy_AddOne();
		try {
			for (int t=0;t<250;t++) {
				int[] input = p.randomInput(8);
				int param = Utils.random.nextInt(4) + 1;
				lang.process(program, ARSearch.nArrays, ARSearch.nVars, input, param);

				//				System.out.print("Stab: ");
				//				for (int i:out){
				//					System.out.print(i + " ");
				//				}
				//				System.out.println();
			}
		}catch(Exception e) {
			return false;
		}
		return true;
	}

	public static int[][] clone(int[][] in) {
		int[][] out = new int[in.length][in[0].length];
		for (int i=0;i<out.length;i++) {
			for (int x=0;x<out[0].length;x++) {
				out[i][x] = in[i][x];
			}
		}
		return out;
	}
}
