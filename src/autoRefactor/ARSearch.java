package autoRefactor;

import humanUsefulProblems.Problem3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import main.MainClass;
import main.Utils;
import main.Gridlang;
import main.CorpusGenerator;
import main.LogBox;

import problems.PEasy_AddOne;
import problems.Problem;

public class ARSearch {
	static double sumTime = 0;
	static double timeCount = 0;
	static long avrgTime = 0;
	static long nProgramsFound = 0;
	static long nProgramsAttempted = 0;
	static long nProgramsPerfectlyMatched = 0;
	//	static double nFound = 0;

	private static ExecutorService executor;


	private static Vector<Callable<ARLimitCheckerWorker>> allTasks;


	private static ArrayList<Future<ARLimitCheckerWorker>> results;


	public static boolean[] perfectMatch;
	public static int[][] foundProgramIndices;
	public static boolean[] boundsEvaluated;
	public static boolean[] boundsSuccess;

	public static int nLines = 9;
	public static int nVars = 4;
	public static int nArrays = 0;
	public static ArrayList<int[]> opts;

	/**
	 * You're essentially looking at depth^nLines
	 * You can explore around 1,000,000 programs per core per second
	 * Maybe try around 100,000 therefore?
	 * 7^6 ~= 117,000
	 */
	public static int param_explorationDepth = 4;
	public static int param_explorationDepth_human = 6;
	public static int param_testsPerWorker = 8;
	public static int param_nExamples = 10;
	public static int param_repeatsOfPrograms = 5;
	private static int param_maxNNTrainingSize = 20*1000;//20k seems to be rather large, tensorflow throws memory warnings

	private static int responseLength;

	private static LogBox successLog;

	public static boolean param_depthFirstSearch = false;

	private static double sumEvals;

	private static int countEvals;

	public static boolean param_displaceSearchWeightings = false;

	public static void singlePass_TestAndHuman(){
		//		LogBox successLog = new LogBox("SearchSuccessLog_TestAndHuman");
		if (successLog == null){//First iteration only. Subsequents can re-use
			successLog = new LogBox("SearchSuccessLog_TestAndHuman");
		}

		String successTesting = ARSearch.cyclicProcess(1, 0,false);
		String resultString = "Depth: " + param_explorationDepth + "/" + param_explorationDepth_human +  
				" Test: " + successTesting;
		successTesting = ARSearch.cyclicProcess(2, 0,false);
		resultString += " Human: " + successTesting;
		System.out.println(resultString);
		successLog.takeLine(resultString);
	}

	public static void singlePass_TestAndHuman_marked(){
		//		LogBox successLog = new LogBox("SearchSuccessLog_TestAndHuman");
		if (successLog == null){//First iteration only. Subsequents can re-use
			successLog = new LogBox("SearchSuccessLog_TestAndHuman_alternate");
		}

		String successTesting = ARSearch.cyclicProcess(1, 0,false);
		String resultString = "Depth: " + param_explorationDepth + "/" + param_explorationDepth_human +  
				" Test: " + successTesting;
		successTesting = ARSearch.cyclicProcess(2, 0,false);
		resultString += " Human: " + successTesting;
		System.out.println(resultString);
		successLog.takeLine(resultString);
	}
	public static void singlePass_TestOnly(){
		LogBox successLog = new LogBox("SearchSuccessLog_TestOnly");
		String successTesting = ARSearch.cyclicProcess(1, 0,false);
		String resultString = " Test: " + successTesting;
		System.out.println(resultString);
		successLog.takeLine(resultString);
	}
	public static void singlePass_HumanOnly(int itr){
		LogBox successLog = new LogBox("SearchSuccessLog_TestOnly");
		String successTesting = ARSearch.cyclicProcess(2, 1,false);
		String resultString = "Human: " + successTesting;
		System.out.println(resultString);
		successLog.takeLine(resultString);
	}
	public static void singlePass_NoReplacement(){
		File f = new File("featuresComplete.log");
		if (f.exists()){f.delete();}

		LogBox successLog = new LogBox("SearchSuccessLog");
		pauseOp();
		long time = System.currentTimeMillis();
		String successHuman = ARSearch.cyclicProcess(2, 0,false);
		String successVal = ARSearch.cyclicProcess(3, 0,false);
		String successTraining = ARSearch.cyclicProcess(0, 0,false);
		String successTesting = ARSearch.cyclicProcess(1, 0,false);
		time = System.currentTimeMillis() - time;
		LogBox log = new LogBox("featuresComplete");
		log.takeLine("done");
		String resultString = "Depth: " + param_explorationDepth + "/" + param_explorationDepth_human + " Train: " + successTraining + " Test: " + successTesting+ " Human: " + successHuman
				+ " Val: " + successVal + " time " + Utils.commas(time) + " ms";
		System.out.println(resultString);
		successLog.takeLine(resultString);
	}

	public static void singlePassNonTraining(){
		File f = new File("featuresComplete.log");
		if (f.exists()){f.delete();}

		if (successLog == null){//First iteration only. Subsequents can re-use
			successLog = new LogBox("SearchSuccessLog");
		}

		pauseOp();
		long time = System.currentTimeMillis();
		String successHuman = ARSearch.cyclicProcess(2, 0,true);
		String successVal = ARSearch.cyclicProcess(3, 0,true);
		//		String successTraining = ARSearch.cyclicProcess(0, 0,true);
		String successTesting = ARSearch.cyclicProcess(1, 0,true);
		time = System.currentTimeMillis() - time;
		LogBox log = new LogBox("featuresComplete");
		log.takeLine("done");
		String resultString = "Depth: " + param_explorationDepth + "/" + param_explorationDepth_human +  " Test: " + successTesting+ " Human: " + successHuman
				+ " Val: " + successVal + " time " + Utils.commas(time) + " ms";
		System.out.println(resultString);
		successLog.takeLine(resultString);
	}
	public static void singlePass(int itr){
		File f = new File("featuresComplete.log");
		if (f.exists()){f.delete();}

		if (successLog == null){//First iteration only. Subsequents can re-use
			successLog = new LogBox("SearchSuccessLog");
		}

		pauseOp();
		long time = System.currentTimeMillis();
		String successHuman = ARSearch.cyclicProcess(2, itr,false);
		String successTesting = ARSearch.cyclicProcess(1, itr,false);
		String successTraining = ARSearch.cyclicProcess(0, itr,true);
		String successVal = ARSearch.cyclicProcess(3, itr,true);
		time = System.currentTimeMillis() - time;
		LogBox log = new LogBox("featuresComplete");
		log.takeLine("done");
		String resultString = "Depth: " + param_explorationDepth + "/" + param_explorationDepth_human + " Train: " + successTraining + " Test: " + successTesting+ " Human: " + successHuman
				+ " Val: " + successVal + " time " + Utils.commas(time) + " ms";
		System.out.println(resultString);
		successLog.takeLine(resultString);
	}


	public static void pauseOp(){
		//Now wait for Tensorflow to finish writing
		while(true){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			File transFolder = new File(".");
			File[] subfiles = transFolder.listFiles();
			boolean foundDone = false;
			for (File f:subfiles){
				if (f.getName().contains("featuresComplete.log")){
					foundDone = true;
				}
			}
			if (!foundDone){break;}
			System.out.println("Waiting");
		}

	}

	public static void generateNNParameterFiles(){
		Gridlang lang = new Gridlang();
		opts = lang.getMaximalOptions(nVars, nArrays);

		//Save the label count to disk, to allow the NN to build a dummy for placeholder filling
		File f = new File("nLabels.log");
		FileWriter w = null;
		try {
			w = new FileWriter(f);
			w.write("" + opts.size());
			w.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				w.close();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		f = new File("nOutputLayers.log");
		w = null;
		try {
			w = new FileWriter(f);
			w.write("" + nLines);
			w.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				w.close();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static String cyclicProcess_NoReplacement(int training,int iteration){
		return cyclicProcess(training,iteration,false);
	}
	public static String cyclicProcess_Replacement(int training,int iteration){
		return cyclicProcess(training,iteration,true);
	}
	public static String cyclicProcess(int training,int iteration,boolean performReplacements){
		System.out.println("Starting cyclic process. Training flag value " + training);
		String trainingPostfix = "";
		if (training == 0) {
			trainingPostfix = "training";
		}else if (training == 1) {
			trainingPostfix = "testing";
		}else if (training == 2) {
			trainingPostfix = "human";
		}else if (training == 3) {
			trainingPostfix = "val";
		}

		//Load what the NN just returned
		//		ArrayList<double[]> nnWeightings = new ArrayList<double[]>();
		ArrayList<int[]> labels = new ArrayList<int[]>();
		readLabelsOnly(training, labels);
		System.out.println("Labels loaded.");

		Gridlang lang = new Gridlang();
		opts = lang.getMaximalOptions(nVars, nArrays);
		int nProgramsTotal = labels.size();


		System.out.println("nPrograms total: " + nProgramsTotal);

		//Save the label count to disk, to allow the NN to build a dummy for placeholder filling
		File f = new File("nLabels.log");
		FileWriter w = null;
		try {
			w = new FileWriter(f);
			w.write("" + opts.size());
			w.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				w.close();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		f = new File("nOutputLayers.log");
		w = null;
		try {
			w = new FileWriter(f);
			w.write("" + nLines);
			w.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				w.close();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}

		boundsEvaluated = new boolean[nProgramsTotal];
		boundsSuccess = new boolean[nProgramsTotal];
		perfectMatch = new boolean[nProgramsTotal];
		foundProgramIndices = new int[nProgramsTotal][];


		System.out.println("Starting subthread generation...");

		long lastTime = 0;
		int position = 0;
		int nThreads = (int) (Runtime.getRuntime().availableProcessors() - 1);
		//		nThreads = 1;
		int nWorkersToBuildMax = nThreads;
		while(true) {
			long time = System.currentTimeMillis();
			if (executor != null && !executor.isShutdown()) {
				executor.shutdownNow();
			}

			ArrayList<ARLimitCheckerWorker> spawningPool = new ArrayList<ARLimitCheckerWorker>();
			while(position < nProgramsTotal && spawningPool.size() < nWorkersToBuildMax){
				//				ArrayList<double[]> nnWeightingsSubset = new ArrayList<double[]>();
				ArrayList<int[]> labelsSubset = new ArrayList<int[]>();
				ArrayList<Integer> indices = new ArrayList<Integer>();
				while(labelsSubset.size() < param_testsPerWorker){
					if (position >= nProgramsTotal){break;}
					labelsSubset.add(labels.get(position));
					indices.add(position);
					//					for (int i=0;i<nLines;i++){
					//						nnWeightingsSubset.add(nnWeightings.get((position*nLines) + i));
					//					}
					position += 1;
				}

				ArrayList<double[]> responseSubset = new ArrayList<double[]>();
				ArrayList<String> responseLog = new ArrayList<String>();
				ARLimitCheckerWorker worker = new ARLimitCheckerWorker(
						training,
						labelsSubset,
						responseSubset,
						indices, responseLog);
				spawningPool.add(worker);

				if (position >= nProgramsTotal){break;}
			}

			allTasks = new Vector<Callable<ARLimitCheckerWorker>>();
			executor = Executors.newFixedThreadPool(nThreads);
			allTasks.addAll(spawningPool);

			double avrgSearch = sumEvals/countEvals;
			long lAvrgSearch = (long)avrgSearch;
			System.out.println("Produced " + allTasks.size() + " workers. Current index: " + position + " nThreads: " + nThreads
					+ " lastTime " + Utils.commas(lastTime) + "ms avrgSearched: " + lAvrgSearch);
			try {
				results = new ArrayList<Future<ARLimitCheckerWorker>>(
						executor.invokeAll(allTasks, 180, TimeUnit.MINUTES));
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(0);
			}

			executor.shutdownNow();
			try {
				while (!executor.awaitTermination(1, TimeUnit.MINUTES)) {
					executor.shutdownNow();
				}
				// Are threads locking on an object, and then being halted, no
				// unlocking is performed..
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(0);
			}

			ArrayList<ARLimitCheckerWorker> assembledWorkers = new ArrayList<ARLimitCheckerWorker>();
			for (Future<ARLimitCheckerWorker> future:results){
				try {
					assembledWorkers.add(future.get().worker);
				} catch (ExecutionException | InterruptedException e) {
					e.printStackTrace();
					System.exit(0);
				}
			}

			//			for (ARLimitCheckerWorker worker:assembledWorkers){
			//				for (int i=0;i<worker.indices.size();i++){
			//					int index = worker.indices.get(i);
			//					responses[index] = worker.responses.get(i);
			//				}
			//			}

			if (position >= nProgramsTotal){break;}

			time = System.currentTimeMillis() - time;
			lastTime = time;
		}

		System.out.println("Search complete");

		//Human training gets special output
		LogBox humanSuccesLog = new LogBox("human_success_log_itr_" + iteration);
		if (training == 2){
			System.out.println("Human useful program success rate:");
			ArrayList<Problem3> p3Set = MainClass.getP3Set();
			if (boundsSuccess.length == p3Set.size()){
				for (int i=0;i<p3Set.size();i++){
					String str = boundsSuccess[i] + " " + p3Set.get(i).getClass().getSimpleName();
					humanSuccesLog.takeLine(str);
					System.out.println(str);
				}
			}else{
				System.out.println("Length mismatch between solved programs and library programs");
				for (int i=0;i<p3Set.size();i++){
					String str = i + " " + boundsSuccess[i];
					humanSuccesLog.takeLine(str);
					System.out.println(str);
				}
			}
		}

		LogBox refactorLog = new LogBox("RefactorLog");
		double successes = 0;
		double perfect = 0;
		double nReplaced = 0;
		LogBox featureLog = null;
		LogBox reseedLog = null;
		if (training == 0 || training == 3) {
			saveExistingLabels("master_labels_" + trainingPostfix + ".log",iteration);
			featureLog = new LogBox("master_labels_" + trainingPostfix);
		}
		if (training == 0){
			reseedLog = new LogBox("reseedPrograms");
		}
		for (int i=0;i<boundsSuccess.length;i++){
			if (boundsSuccess[i]){
				successes += 1;

				//Write out the programs you found, and only those you found,
				//	to generate a whole new corpus using these as seeds
				if (training == 0 || training == 3){
					String encodedProgram = "";
					for (int k:foundProgramIndices[i]) {
						encodedProgram += k + " ";
					}
					encodedProgram = encodedProgram.substring(0,encodedProgram.length()-1);
					if (training == 0){
						reseedLog.takeLine(encodedProgram);
					}
				}

				if ((training == 0 || training == 3) && performReplacements) {
					if (!perfectMatch[i]) {
						refactorLog.takeLine("Original:");
						for (int k: labels.get(i)) {
							String line = "";
							for (int q:opts.get(k)) {
								line += q + " ";
							}
							refactorLog.takeLine(line);
						}
						refactorLog.takeLine("Refactored:");
						for (int k:foundProgramIndices[i]) {
							String line = "";
							for (int q:opts.get(k)) {
								line += q + " ";
							}
							refactorLog.takeLine(line);
						}
						refactorLog.takeLine("");
						nReplaced += 1;
					}

					String programIndicesStr = "";
					for (int k:foundProgramIndices[i]){
						programIndicesStr += k + " ";
					}
					programIndicesStr = programIndicesStr.substring(0, programIndicesStr.length()-1);
					featureLog.takeLine(programIndicesStr);
				}

				if (perfectMatch[i]){
					//						System.out.println(i + " perfect match");
				}else{
					String str = i + " : ";
					int[] pastProgram = labels.get(i);
					int[] newProgram =  foundProgramIndices[i];

					int[][] encPast = new int[nLines][5];
					for (int j=0;j<nLines;j++){
						encPast[j] = opts.get(labels.get(i)[j]);
					}
					int[][] encNew = new int[nLines][5];
					for (int j=0;j<nLines;j++){
						encNew[j] = opts.get(foundProgramIndices[i][j]);
					}
					boolean match = CorpusGenerator.assertFunctionalities(new PEasy_AddOne(), encPast, encNew, nArrays, nVars);
					str += " match: " + match + " ";

					for (int k:pastProgram){
						str += k + " ";
					}
					str += " -> ";
					for (int k:newProgram){
						str += k + " ";
					}
					//					System.out.println(str);
				}
			}else{
				if ((training == 0 || training == 3) && performReplacements) {
					String programIndicesStr = "";
					for (int k:labels.get(i)){
						programIndicesStr += k + " ";
					}
					programIndicesStr = programIndicesStr.substring(0, programIndicesStr.length()-1);
					featureLog.takeLine(programIndicesStr);
				}
			}
			if (perfectMatch[i]){
				perfect += 1;
			}

			//			String outStr = i + " found " + boundsSuccess[i] + " perfect " + perfectMatch[i] + " ";
			//			if (boundsSuccess[i]){
			//				int[] found = foundProgramIndices[i];
			//				for (int j=0;j<found.length;j++){
			//					outStr += found[j] + " ";
			//				}
			//			}
			//			System.out.println(outStr);
		}


		LogBox verboseSuccesses = new LogBox("VerboseSuccesses_" + trainingPostfix+ "_itr" + iteration);
		for (int i=0;i<boundsSuccess.length;i++){
			verboseSuccesses.takeLine("" + boundsSuccess[i]);
		}
		LogBox programsFoundLog = new LogBox("ProgramsFound_" + trainingPostfix + "_itr" + iteration);
		for (int i=0;i<boundsSuccess.length;i++){
			String foundProgram = "";
			if (foundProgramIndices[i] == null){
				foundProgram += "null";
			}else{
				for (int k:foundProgramIndices[i]){
					foundProgram += k + " ";
				}
			}
			programsFoundLog.takeLine(foundProgram);
		}


		if (training != 0 && training != 3){
			boolean[] hasLoop = new boolean[boundsSuccess.length];
			int nWhiles = 0;
			int nLoops = 0;
			for (int i=0;i<boundsSuccess.length;i++){
				boolean pHasLoop = false;
				for (int k:labels.get(i)){
					int[] line = opts.get(k);

					if (param_displaceSearchWeightings){
						if (line[0] == -2 || line[1] == -3){
							pHasLoop = true;
						}
						if (line[3] == -3){
							nWhiles += 1;
						}
					}
				}
				if (pHasLoop){
					nLoops += 1;
					hasLoop[i] = true;
				}
			}
			System.out.println("nLoops: " + nLoops +  " / " + boundsSuccess.length);
			double pLoop = nLoops/boundsSuccess.length;
			int nLoopsFound = 1;
			int nLoopsInSuccesses = 0;
			int nTargetLoopsInSuccesses = 0;
			int nSuccesses = 0;
			for (int i=0;i<boundsSuccess.length;i++){
				if (boundsSuccess[i]){
					nSuccesses += 1;
					boolean pHasLoop = false;
					for (int k:foundProgramIndices[i]){
						int[] line = opts.get(k);
						if (line[0] == -2 || line[1] == -3){
							pHasLoop = true;
						}
					}
					if (pHasLoop){
						nLoopsInSuccesses += 1;
					}
					if (hasLoop[i]){
						nTargetLoopsInSuccesses += 1;
					}
				}
			}
			System.out.println("nLoops in successes: " + nLoopsInSuccesses + " in labels: " + nTargetLoopsInSuccesses + " totalSuccesses: " + nSuccesses);
		}

		String resultStr = "Success: " + (successes/boundsSuccess.length) + " (" + (perfect/boundsSuccess.length) + ")";
		resultStr += "Replacement p: " + (nReplaced/boundsSuccess.length);
		System.out.println(resultStr);
		return resultStr;
	}

	private static void saveExistingLabels(String string, int iteration) {
		ArrayList<String> fileHistory = new ArrayList<String>();
		File file = new File(string);
		if (file.exists()){
			try {
				BufferedReader br = new BufferedReader(new FileReader(file));

				String st;
				while ((st = br.readLine()) != null) {
					fileHistory.add(st);
				}
				br.close();
			}catch(Exception e) {
				e.printStackTrace()
				;
			}
		}else {
			throw new Error("Non-existant file: " + string);
		}
		LogBox log = new LogBox(string + "_saved_" + iteration);
		for (String s:fileHistory){
			log.takeLine(s);
		}
	}

	public static void catFeatureFiles(String oldFile,ArrayList<String> additions,long shuffleSeed){
		Random r = new Random();
		r.setSeed(shuffleSeed);

		ArrayList<String> fileHistory = new ArrayList<String>();
		File file = new File(oldFile);
		if (file.exists()){
			try {
				BufferedReader br = new BufferedReader(new FileReader(file));

				String st;
				while ((st = br.readLine()) != null) {
					fileHistory.add(st);
				}
				br.close();
			}catch(Exception e) {
				e.printStackTrace()
				;
			}
		}

		fileHistory.addAll(additions);

		ArrayList<ArrayList<String>> fileBuffers = new ArrayList<ArrayList<String>>();
		ArrayList<String> local = new ArrayList<String>();
		for (String s:fileHistory){
			local.add(s);
			if (s.contentEquals("end")){
				fileBuffers.add(local);
				local = new ArrayList<String>();
			}
		}

		if (fileBuffers.size() >= param_maxNNTrainingSize ){
			Collections.shuffle(fileBuffers,r);
			while(fileBuffers.size() > param_maxNNTrainingSize){
				fileBuffers.remove(fileBuffers.size()-1);
			}
		}

		LogBox rewriter = new LogBox("");
		rewriter.filename = oldFile;
		for (ArrayList<String> l2:fileBuffers){
			for (String s:l2){
				rewriter.takeLine(s);
			}
		}
	}
	public static void catLabelFiles(String oldFile,ArrayList<String> additions,long shuffleSeed){
		Random r = new Random();
		r.setSeed(shuffleSeed);

		ArrayList<String> fileHistory = new ArrayList<String>();
		File file = new File(oldFile);
		if (file.exists()){
			try {
				BufferedReader br = new BufferedReader(new FileReader(file));

				String st;
				while ((st = br.readLine()) != null) {
					fileHistory.add(st);
				}
				br.close();
			}catch(Exception e) {
				e.printStackTrace()
				;
			}
		}

		fileHistory.addAll(additions);

		if (fileHistory.size() >= param_maxNNTrainingSize ){
			Collections.shuffle(fileHistory,r);
			while(fileHistory.size() > param_maxNNTrainingSize){
				fileHistory.remove(fileHistory.size()-1);
			}
		}

		LogBox rewriter = new LogBox("");
		rewriter.filename = oldFile;
		for (String s:fileHistory){
			rewriter.takeLine(s);
		}
	}

	/**
	 * Get the data the NN just returned
	 * Also re-loads the data we're looking for, and the record of what we've got
	 * Re-loading avoids risk of dropping stuff if we allow resumption from checkpoint 
	 * 
	 * @param training
	 * @param targetLabels
	 * @param NNWeightings
	 */
	public static void readNNOutput(
			int training,
			ArrayList<int[]> targetLabels,
			ArrayList<double[]> NNWeightings
			){
		String subpostfix = "";
		if (training == 0) {
			subpostfix = "training";
		}else if (training == 1) {
			subpostfix = "testing";
		}else if (training == 2) {
			subpostfix = "human";
		}else if (training == 3) {
			subpostfix = "validation";
		}
		File file;

		ArrayList<String> fileLabels = new ArrayList<String>();
		file = new File("master_labels_" + subpostfix + ".log");
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));

			String st;
			while ((st = br.readLine()) != null) {
				fileLabels.add(st);
			}
			br.close();
		}catch(Exception e) {
			e.printStackTrace()
			;
		}

		ArrayList<String> fileNNWeightings = new ArrayList<String>();
		file = new File("nn_weightings_" + subpostfix + ".log");
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));

			String st;
			while ((st = br.readLine()) != null) {
				fileNNWeightings.add(st);
			}
			br.close();
		}catch(Exception e) {
			e.printStackTrace()
			;
		}

		for (int i=0;i<fileLabels.size();i++){
			targetLabels.add(toIntArray(fileLabels.get(i)));
			for (int k=0;k<nLines;k++){
				NNWeightings.add(toDoubleArray(fileNNWeightings.get((i*nLines)+k)));
			}
		}
	}

	public static void readLabelsOnly(
			int training,
			ArrayList<int[]> targetLabels
			){
		String subpostfix = "";
		if (training == 0) {
			subpostfix = "training";
		}else if (training == 1) {
			subpostfix = "testing";
		}else if (training == 2) {
			subpostfix = "human";
		}else if (training == 3) {
			subpostfix = "val";
		}
		File file;

		ArrayList<String> fileLabels = new ArrayList<String>();
		file = new File("master_labels_" + subpostfix + ".log");
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));

			String st;
			while ((st = br.readLine()) != null) {
				fileLabels.add(st);
			}
			br.close();
		}catch(Exception e) {
			e.printStackTrace()
			;
		}

		for (int i=0;i<fileLabels.size();i++){
			targetLabels.add(toIntArray(fileLabels.get(i)));
		}
	}

	public static void featuriseSet_calledByWorker_queue(
			//			ArrayList<double[]> nnWeightings
			int trainingFlag,
			ArrayList<int[]> programsLabels
			,ArrayList<double[]> responses
			,ArrayList<Integer> indices
			,ArrayList<String> responseLog
			){
		long time = System.currentTimeMillis();


		String subpostfix = "";
		if (trainingFlag == 0) {
			subpostfix = "training";
		}else if (trainingFlag == 1) {
			subpostfix = "testing";
		}else if (trainingFlag == 2) {
			subpostfix = "human";
		}else if (trainingFlag == 3) {
			subpostfix = "validation";
		}
		File file;

		ArrayList<String> fileNNWeightings = new ArrayList<String>();
		file = new File("nn_weightings_" + subpostfix + ".log");
		int ind = 0;
		int startIndex = indices.get(0)*nLines;
		int endIndex = (indices.get(indices.size()-1)+1)*nLines;
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));

			String st;
			while ((st = br.readLine()) != null) {
				if (ind >= startIndex && ind <= endIndex){
					fileNNWeightings.add(st);
				}
				ind += 1;
			}
			br.close();
		}catch(Exception e) {
			e.printStackTrace()
			;
		}

		ArrayList<double[][]> nnWeightings = new ArrayList<double[][]>();
		for (int i=0;i<fileNNWeightings.size();i++){
			if (fileNNWeightings.get(i) != null){
				nnWeightings.add(toIndexedDoubleArray(fileNNWeightings.get(i)));
			}
		}

		for (double[][] weightings:nnWeightings){
			Arrays.sort(weightings,new IndexArrayComparator());
		}

		Gridlang interpreter = new Gridlang();


		for (int k=0;k<programsLabels.size();k++){
			int fl = indices.get(k);


			List<double[][]> nnWeights = nnWeightings.subList((k*nLines), (k+1)*(nLines)); 
			ArrayList<ArrayList<Integer>> boundPositions = new ArrayList<ArrayList<Integer>>();
			for (int i=0;i<nLines;i++){boundPositions.add(new ArrayList<Integer>());}
			int depth = param_explorationDepth;
			if (trainingFlag == 2){depth = param_explorationDepth_human;}

			int overdepth = depth + 1;

			int nToOversample = (int) Math.pow(overdepth, nLines); 
			long nToSearch = (int) Math.pow(depth, nLines); 

			PriorityQueue<QueueElement> q = new PriorityQueue<QueueElement>(nToOversample,new QueueElement(null,0));
			//Priority queue established,
			//Add a rectangle from the ordered weightings which is larger than the number you want to search
			int[] currentLocation = new int[nLines];
			exhaustionLoop:
				while(true){
					double v = 0;
					for (int i=0;i<currentLocation.length;i++){
						v += nnWeights.get(i)[currentLocation[i]][0];
					}
					q.add(new QueueElement(currentLocation.clone(),v));

					currentLocation[0] += 1;
					for (int i=0;i<currentLocation.length;i++) {
						if (currentLocation[i] >= overdepth) {
							currentLocation[i] = 0;
							if (i == currentLocation.length-1) {
								break exhaustionLoop;
							}
							currentLocation[i + 1] += 1;
							//						System.out.println("Incrementing " + i);
						}
					}
				}


			//Define your target program
			int[] targetProgramIndices = programsLabels.get(k);
			int[][] targetProgram = new int[nLines][];
			for (int i=0;i<nLines;i++) {
				targetProgram[i] = opts.get(targetProgramIndices[i]);
			}

			boolean foundFunctionalMatch = false;
			int[] foundProgramLocation = null;
			int index = 0;
			QueueElement e;
			//Queue now filled. Process elements
			outer:
				for (int s=0;s<nToSearch;s++){
					e = q.poll();
					if (e == null){break;}
					int[][] program = new int[nLines][];
					for (int i=0;i<program.length;i++){
						if (e == null){throw new Error("e is null");}
						if (e.position == null){throw new Error("e.pos is null");}
						index = (int) nnWeightings.get(i)[e.position[i]][1];
						program[i] = opts.get(index);
					}

					boolean areIdentical = CorpusGenerator.assertFunctionalities(
							new PEasy_AddOne(),program,targetProgram,nArrays,nVars);
					//										System.out.println("Program match found, identical: " + match + " fulfill check: " + areIdentical);
					if (areIdentical){
						foundFunctionalMatch = true;
						foundProgramLocation = new int[currentLocation.length];
						for (int i=0;i<program.length;i++){
							foundProgramLocation[i] = (int) nnWeightings.get(i)[e.position[i]][1];
						}
						break outer;
					}
				}

			if (foundFunctionalMatch){
				int[][] program = new int[nLines][];
				//Regenerate the program real quick
				for (int i=0;i<program.length;i++) {
					program[i] = opts.get(foundProgramLocation[i]);
				}
				boolean reallyPassed = CorpusGenerator.checkStability(program, nVars, nArrays, interpreter);
				foundFunctionalMatch = reallyPassed;
				if (!reallyPassed) {
					System.out.println("Function match found but discarded due to stability checking");
				}
			}

			boolean found = false;
			if (foundFunctionalMatch){
				foundProgramIndices[fl] = foundProgramLocation;
				found = true;
			}

			//			totalEvaluations += 1;

			if (found) {//Flag for functional match
				nProgramsFound += 1;
				boundsEvaluated[fl] = true;
				boundsSuccess[fl] = true;
				//				System.out.println("Functional match");

				boolean perfect = true;
				for (int i=0;i<targetProgramIndices.length;i++){
					if (targetProgramIndices[i] != foundProgramIndices[fl][i]){
						perfect = false;
					}
				}
				if (perfect){
					perfectMatch[fl] = true;
					nProgramsPerfectlyMatched += 1;
				}

			}else{
				boundsEvaluated[fl] = true;
				boundsSuccess[fl] = false;
				//				System.out.println("Non-match functionally");
			}
		}
		time = System.currentTimeMillis() - time;
		sumTime += time;
		timeCount += 1;
		avrgTime = (long)((sumTime/timeCount)/1000);

		double pFind = nProgramsFound;
		pFind /= nProgramsAttempted;
		double pPerfectFind = nProgramsPerfectlyMatched;
		pPerfectFind /= nProgramsAttempted;
		String outStr = "Set of " + programsLabels.size() + " processed " 
				+ " time(ms):" + time
				+ " pFind: " + pFind + " pPerfect: " + pPerfectFind;
		responseLog.add(outStr);
		//		System.out.println(outStr);
	}
	public static void featuriseSet_calledByWorker_jagged(
			//			ArrayList<double[]> nnWeightings
			int trainingFlag,
			ArrayList<int[]> programsLabels
			,ArrayList<double[]> responses
			,ArrayList<Integer> indices
			,ArrayList<String> responseLog
			){
		long time = System.currentTimeMillis();


		String subpostfix = "";
		if (trainingFlag == 0) {
			subpostfix = "training";
		}else if (trainingFlag == 1) {
			subpostfix = "testing";
		}else if (trainingFlag == 2) {
			subpostfix = "human";
		}else if (trainingFlag == 3) {
			subpostfix = "validation";
		}
		File file;

		ArrayList<String> fileNNWeightings = new ArrayList<String>();
		file = new File("nn_weightings_" + subpostfix + ".log");
		int ind = 0;
		int startIndex = indices.get(0)*nLines;
		int endIndex = (indices.get(indices.size()-1)+1)*nLines;
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));

			String st;
			while ((st = br.readLine()) != null) {
				if (ind >= startIndex && ind <= endIndex){
					fileNNWeightings.add(st);
				}
				ind += 1;
			}
			br.close();
		}catch(Exception e) {
			e.printStackTrace()
			;
		}

		ArrayList<double[][]> nnWeightings = new ArrayList<double[][]>();
		for (int i=0;i<fileNNWeightings.size();i++){
			if (fileNNWeightings.get(i) != null){
				nnWeightings.add(toIndexedDoubleArray(fileNNWeightings.get(i)));
			}
		}

		for (double[][] weightings:nnWeightings){
			Arrays.sort(weightings,new IndexArrayComparator());
		}



		for (int k=0;k<programsLabels.size();k++){
			int fl = indices.get(k);


			List<double[][]> nnWeights = nnWeightings.subList((k*nLines), (k+1)*(nLines)); 
			ArrayList<ArrayList<Integer>> boundPositions = new ArrayList<ArrayList<Integer>>();
			for (int i=0;i<nLines;i++){boundPositions.add(new ArrayList<Integer>());}
			int nToUse = param_explorationDepth*nLines;
			if (trainingFlag == 2){nToUse = param_explorationDepth_human*nLines;}

			//Every jagged row must contain at least one element, the highest-confidence one
			int nToPreAdd = 3;
			for (int i=0;i<nLines;i++){
				for (int j=0;j<nToPreAdd;j++){
					boundPositions.get(i).add(j);
					nToUse -= 1;
				}
				//				double[][] row = nnWeights.get(i);
				//				String rowStr = i + " : ";
				//				for (int j=0;j<10;j++){
				//					rowStr += row[j][0] + " ";
				//				}
				//				System.out.println(rowStr);
			}

			int[] boundEnds = new int[nLines];
			for (int i=0;i<boundEnds.length;i++){
				boundEnds[i] = nToPreAdd-1;
			}
			int nUsed = 0;
			while(nUsed < nToUse){
				int bestInd = 0;
				double bestV = nnWeights.get(0)[boundEnds[0] + 1][0];
				for (int i=1;i<nLines;i++){
					if (nnWeights.get(i)[boundEnds[i] + 1][0] > bestV){
						bestV = nnWeights.get(i)[boundEnds[i] + 1][0];
						bestInd = i;
					}
				}
				boundEnds[bestInd] += 1;
				nUsed += 1;
			}

			//			for (int i=0;i<boundEnds.length;i++){
			//				System.out.println("BoundEnd: " + i + " : " + boundEnds[i]);
			//			}

			int[][] bounds = new int[nLines][];
			int[] targetProgramIndices = programsLabels.get(k);
			int[][] targetProgram = new int[nLines][];
			for (int i=0;i<nLines;i++) {
				targetProgram[i] = opts.get(targetProgramIndices[i]);

				int depth = boundEnds[i]+1;
				int[] row = new int[depth];
				//				String rowStr = "Row: " + i + " : ";
				for (int j=0;j<depth;j++){
					row[j] = (int) nnWeights.get(i)[j][1];
					//					rowStr += nnWeights.get(i)[j][0] + "   ";
				}
				//				System.out.println(rowStr);
				bounds[i] = row;
			}


			nProgramsAttempted += 1;
			//			boolean boundsHadTarget = boundsContainTarget(bounds,targetProgramIndices);
			//			if (boundsHadTarget){
			//				nProgramsFound += 1;
			//				nProgramsPerfectlyMatched += 1;
			//				boundsEvaluated[fl] = true;
			//				boundsSuccess[fl] = true;
			//				perfectMatch[fl] = true;
			//				foundProgramIndices[fl] = targetProgramIndices;
			//				//				System.out.println("Perfect match");
			//				responses.add(null);
			//				continue;
			//			}

			boolean found = process(bounds,targetProgram,fl);
			if (found) {//Flag for functional match
				nProgramsFound += 1;
				boundsEvaluated[fl] = true;
				boundsSuccess[fl] = true;
				//				System.out.println("Functional match");

				boolean perfect = true;
				for (int i=0;i<targetProgramIndices.length;i++){
					if (targetProgramIndices[i] != foundProgramIndices[fl][i]){
						perfect = false;
					}
				}
				if (perfect){
					perfectMatch[fl] = true;
					nProgramsPerfectlyMatched += 1;
				}

			}else{
				boundsEvaluated[fl] = true;
				boundsSuccess[fl] = false;
				//				System.out.println("Non-match functionally");
			}
		}
		time = System.currentTimeMillis() - time;
		sumTime += time;
		timeCount += 1;
		avrgTime = (long)((sumTime/timeCount)/1000);

		double pFind = nProgramsFound;
		pFind /= nProgramsAttempted;
		double pPerfectFind = nProgramsPerfectlyMatched;
		pPerfectFind /= nProgramsAttempted;
		String outStr = "Set of " + programsLabels.size() + " processed " 
				+ " time(ms):" + time
				+ " pFind: " + pFind + " pPerfect: " + pPerfectFind;
		responseLog.add(outStr);
		//		System.out.println(outStr);
	}
	public static void featuriseSet_calledByWorker(
			//			ArrayList<double[]> nnWeightings
			int trainingFlag,
			ArrayList<int[]> programsLabels
			,ArrayList<double[]> responses
			,ArrayList<Integer> indices
			,ArrayList<String> responseLog
			){
		long time = System.currentTimeMillis();

		//TODO
		ArrayList<double[]> nnWeightings = new ArrayList<double[]>();

		String subpostfix = "";
		if (trainingFlag == 0) {
			subpostfix = "training";
		}else if (trainingFlag == 1) {
			subpostfix = "testing";
		}else if (trainingFlag == 2) {
			subpostfix = "human";
		}else if (trainingFlag == 3) {
			subpostfix = "validation";
		}
		File file;

		ArrayList<String> fileNNWeightings = new ArrayList<String>();
		file = new File("nn_weightings_" + subpostfix + ".log");
		int ind = 0;
		int startIndex = indices.get(0)*nLines;
		int endIndex = (indices.get(indices.size()-1)+1)*nLines;
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));

			String st;
			while ((st = br.readLine()) != null) {
				if (ind >= startIndex && ind <= endIndex){
					fileNNWeightings.add(st);
				}
				ind += 1;
			}
			br.close();
		}catch(Exception e) {
			e.printStackTrace()
			;
		}

		for (int i=0;i<fileNNWeightings.size();i++){
			if (fileNNWeightings.get(i) != null){
				nnWeightings.add(toDoubleArray(fileNNWeightings.get(i)));
			}
		}

		for (int k=0;k<programsLabels.size();k++){
			int fl = indices.get(k);

			int[][] bounds = new int[nLines][];
			int[] targetProgramIndices = programsLabels.get(k);
			int[][] targetProgram = new int[nLines][];
			for (int i=0;i<nLines;i++) {
				targetProgram[i] = opts.get(targetProgramIndices[i]);
				int depth = param_explorationDepth;
				if (trainingFlag == 2){
					depth = param_explorationDepth_human;
				}
				if (param_displaceSearchWeightings) {
					if (i == 2){
						depth += 1;
					}
					if (i == 3){
						depth += 1;
					}
					if (i == 8){
						depth -= 1;
					}
					if (i == 9){
						depth -= 1;
					}
				}
				int[] lineBounds = getBounds(nnWeightings.get((k*nLines) + i), depth);
				bounds[i] = lineBounds;
			}


			nProgramsAttempted += 1;
			//			boolean boundsHadTarget = boundsContainTarget(bounds,targetProgramIndices);
			//			if (boundsHadTarget){
			//				nProgramsFound += 1;
			//				nProgramsPerfectlyMatched += 1;
			//				boundsEvaluated[fl] = true;
			//				boundsSuccess[fl] = true;
			//				perfectMatch[fl] = true;
			//				foundProgramIndices[fl] = targetProgramIndices;
			//				//				System.out.println("Perfect match");
			//				responses.add(null);
			//				continue;
			//			}

			boolean found = process(bounds,targetProgram,fl);
			if (found) {//Flag for functional match
				nProgramsFound += 1;
				boundsEvaluated[fl] = true;
				boundsSuccess[fl] = true;
				//				System.out.println("Functional match");

				boolean perfect = true;
				for (int i=0;i<targetProgramIndices.length;i++){
					if (targetProgramIndices[i] != foundProgramIndices[fl][i]){
						perfect = false;
					}
				}
				if (perfect){
					perfectMatch[fl] = true;
					nProgramsPerfectlyMatched += 1;
				}

			}else{
				boundsEvaluated[fl] = true;
				boundsSuccess[fl] = false;
				//				System.out.println("Non-match functionally");
			}
		}
		time = System.currentTimeMillis() - time;
		sumTime += time;
		timeCount += 1;
		avrgTime = (long)((sumTime/timeCount)/1000);

		double pFind = nProgramsFound;
		pFind /= nProgramsAttempted;
		double pPerfectFind = nProgramsPerfectlyMatched;
		pPerfectFind /= nProgramsAttempted;
		double avrgSearch = sumEvals/countEvals;
		long lAvrgSearch = (long)avrgSearch;
		String outStr = "Set of " + programsLabels.size() + " processed " 
				+ " time(ms):" + time
				+ " pFind: " + pFind + " pPerfect: " + pPerfectFind
				+ " avrgSearched " + lAvrgSearch;
		responseLog.add(outStr);
		//		System.out.println(outStr);
	}



	private static int[][] splitArray(int[] is, int examples) {
		int subLen = is.length/examples;
		int[][] split = new int[examples][subLen];
		if (subLen*examples != is.length) {throw new Error();}

		int x = 0;
		int y = 0;
		for (int i=0;i<is.length;i++) {
			split[y][x] = is[i];
			x += 1;
			if (x >= subLen) {
				x = 0;
				y += 1;
			}
		}
		return split;
	}

	public static int[] getBounds(double[] weightings,int nBounds) {
		boolean[] used = new boolean[weightings.length];
		int[] chosen = new int[nBounds];
		for (int b=0;b<nBounds;b++) {
			double highest = -1000000000;
			int bestIndex = -1;
			for (int i=0;i<weightings.length;i++) {
				if (used[i]) {continue;}
				if (weightings[i] > highest) {
					highest = weightings[i];
					bestIndex = i;
				}
			}
			chosen[b] = bestIndex;
			used[bestIndex] = true;
		}
		return chosen;
	}

	public static int[] toIntArray(String str) {
		String[] split = str.split(" ");
		int[] reply = new int[split.length];
		for (int i=0;i<split.length;i++) {
			reply[i] = Integer.parseInt(split[i]);
		}

		return reply;
	}
	public static double[] toDoubleArray(String str) {
		String[] split = str.split(" ");
		double[] reply = new double[split.length];
		for (int i=0;i<split.length;i++) {
			reply[i] = Double.parseDouble(split[i]);
		}

		return reply;
	}
	public static double[][] toIndexedDoubleArray(String str) {
		String[] split = str.split(" ");
		double[][] reply = new double[split.length][2];
		for (int i=0;i<split.length;i++) {
			reply[i][0] = Double.parseDouble(split[i]);
			reply[i][1] = i;
		}

		return reply;
	}

	public static boolean boundsContainTarget(int[][] bounds,int[] targetProgramIndices){
		outer:
			for (int i=0;i<targetProgramIndices.length;i++){
				for (int j:bounds[i]){
					if (targetProgramIndices[i] == j){
						continue outer;
					}
				}
				//Got here because we didn't hit the continue
				return false;
			}
	return true;
	}

	public static boolean process(int[][] bounds,int[][] targetProgram,int programIndex){
		if (param_depthFirstSearch ) {
			return process_depthFirst(bounds,targetProgram,programIndex);
		}else {
			return process_breadthFirst(bounds,targetProgram,programIndex);
		}
	}
	public static boolean process_breadthFirst(int[][] bounds,int[][] targetProgram,int programIndex){
		int[] currentLocation = new int[bounds.length];
		int[][] program = new int[bounds.length][];
		Gridlang interpreter = new Gridlang();

		boolean foundFunctionalMatch = false;
		double totalEvaluations = 0;

		int[] foundProgramLocation = null;

//		long time = System.currentTimeMillis();
		//		System.out.println("Beginning breadth first search...");
		boolean doSearch = true;
		exhaustionLoop:
			for (int sd=0;sd<bounds[0].length+1;sd++) {
				innerLoop:
					while(true){
						//Only search if you've not already covered it
						//Since search is systematic, we can autodetect ones we should have covered
						//based on depth of search and deepest point in currentLocation
						if (doSearch) {
							for (int i=0;i<program.length;i++) {
								program[i] = opts.get(bounds[i][currentLocation[i]]);
							}
							//							System.out.print("Searching: ");
							//							for (int q:currentLocation) {
							//								System.out.print(q + " ");
							//							}
							//							System.out.println();

							totalEvaluations += 1;

							boolean areIdentical = CorpusGenerator.assertFunctionalities(
									new PEasy_AddOne(),program,targetProgram,nArrays,nVars);
							//										System.out.println("Program match found, identical: " + match + " fulfill check: " + areIdentical);
							if (areIdentical){
								foundFunctionalMatch = true;
								foundProgramLocation = new int[currentLocation.length];
								for (int k=0;k<foundProgramLocation.length;k++){
									foundProgramLocation[k] = bounds[k][currentLocation[k]];
								}
								System.out.print("Solution found: ");
								for (int k:currentLocation) {
									System.out.print(k + " ");
								}
								System.out.println();
								break exhaustionLoop;
							}
						}


						int max = 0;
						currentLocation[0] += 1;
						for (int i=0;i<currentLocation.length;i++) {
							if (currentLocation[i] >= bounds[i].length) {
								currentLocation[i] = 0;
								if (i == currentLocation.length-1) {
									break innerLoop;
								}
								currentLocation[i + 1] += 1;
								//						System.out.println("Incrementing " + i);
							}
							if (currentLocation[i] > max) {
								max = currentLocation[i];
							}
						}
						doSearch = max == sd - 1;
					}
			}

//		long dt = System.currentTimeMillis() - time;
//				System.out.println("Breadth first evaluations: " + totalEvaluations  + " dt " + dt);

		if (foundFunctionalMatch){
			//Regenerate the program real quick
			for (int i=0;i<program.length;i++) {
				program[i] = opts.get(foundProgramLocation[i]);
			}
			boolean reallyPassed = CorpusGenerator.checkStability(program, nVars, nArrays, interpreter);
			foundFunctionalMatch = reallyPassed;
			if (!reallyPassed) {
				System.out.println("Function match found but discarded due to stability checking");
			}
		}

		if (foundFunctionalMatch){
			foundProgramIndices[programIndex] = foundProgramLocation;
			return true;
		}
		else{
			return false;
		}
	}

	public static boolean process_depthFirst(int[][] bounds,int[][] targetProgram,int programIndex){
		int[] currentLocation = new int[bounds.length];
		int[][] program = new int[bounds.length][];
		Gridlang interpreter = new Gridlang();

		boolean foundFunctionalMatch = false;
		double totalEvaluations = 0;

		int[] foundProgramLocation = null;

		exhaustionLoop:
			while(true){
				for (int i=0;i<program.length;i++) {
					program[i] = opts.get(bounds[i][currentLocation[i]]);
				}

				totalEvaluations += 1;
				boolean areIdentical = CorpusGenerator.assertFunctionalities(
						new PEasy_AddOne(),program,targetProgram,nArrays,nVars);
				//										System.out.println("Program match found, identical: " + match + " fulfill check: " + areIdentical);
				if (areIdentical){
					foundFunctionalMatch = true;
					foundProgramLocation = new int[currentLocation.length];
					for (int k=0;k<foundProgramLocation.length;k++){
						foundProgramLocation[k] = bounds[k][currentLocation[k]];
					}
					break exhaustionLoop;
				}


				currentLocation[0] += 1;
				for (int i=0;i<currentLocation.length;i++) {
					if (currentLocation[i] >= bounds[i].length) {
						currentLocation[i] = 0;
						if (i == currentLocation.length-1) {
							break exhaustionLoop;
						}
						currentLocation[i + 1] += 1;
						//						System.out.println("Incrementing " + i);
					}
				}

				//				for (int i:currentLocation){
				//					System.out.print(i + " ");
				//				}
				//				System.out.println();
			}

		sumEvals += totalEvaluations;
		countEvals += 1;
		//		for (int i=0;i<bounds.length;i++){
		//			System.out.println(i + " : " + bounds[i].length);
		//		}
		//		System.out.println("Depth first evaluations " + totalEvaluations + " found: " + foundFunctionalMatch);

		if (foundFunctionalMatch){
			//Regenerate the program real quick
			for (int i=0;i<program.length;i++) {
				program[i] = opts.get(foundProgramLocation[i]);
			}
			boolean reallyPassed = CorpusGenerator.checkStability(program, nVars, nArrays, interpreter);
			foundFunctionalMatch = reallyPassed;
			if (!reallyPassed) {
				System.out.println("Function match found but discarded due to stability checking");
			}
		}

		if (foundFunctionalMatch){
			foundProgramIndices[programIndex] = foundProgramLocation;
			return true;
		}
		else{
			return false;
		}
	}

	public static boolean process_priorityQueue(int[][] bounds,int[][] targetProgram,int programIndex){
		int[] currentLocation = new int[bounds.length];
		int[][] program = new int[bounds.length][];
		Gridlang interpreter = new Gridlang();

		boolean foundFunctionalMatch = false;
		double totalEvaluations = 0;

		int[] foundProgramLocation = null;

		exhaustionLoop:
			while(true){
				for (int i=0;i<program.length;i++) {
					program[i] = opts.get(bounds[i][currentLocation[i]]);
				}

				totalEvaluations += 1;
				boolean areIdentical = CorpusGenerator.assertFunctionalities(
						new PEasy_AddOne(),program,targetProgram,nArrays,nVars);
				//										System.out.println("Program match found, identical: " + match + " fulfill check: " + areIdentical);
				if (areIdentical){
					foundFunctionalMatch = true;
					foundProgramLocation = new int[currentLocation.length];
					for (int k=0;k<foundProgramLocation.length;k++){
						foundProgramLocation[k] = bounds[k][currentLocation[k]];
					}
					break exhaustionLoop;
				}


				currentLocation[0] += 1;
				for (int i=0;i<currentLocation.length;i++) {
					if (currentLocation[i] >= bounds[i].length) {
						currentLocation[i] = 0;
						if (i == currentLocation.length-1) {
							break exhaustionLoop;
						}
						currentLocation[i + 1] += 1;
						//						System.out.println("Incrementing " + i);
					}
				}

				//				for (int i:currentLocation){
				//					System.out.print(i + " ");
				//				}
				//				System.out.println();
			}

		//		System.out.println("Depth first evaluations " + totalEvaluations);

		if (foundFunctionalMatch){
			//Regenerate the program real quick
			for (int i=0;i<program.length;i++) {
				program[i] = opts.get(foundProgramLocation[i]);
			}
			boolean reallyPassed = CorpusGenerator.checkStability(program, nVars, nArrays, interpreter);
			foundFunctionalMatch = reallyPassed;
			if (!reallyPassed) {
				System.out.println("Function match found but discarded due to stability checking");
			}
		}

		if (foundFunctionalMatch){
			foundProgramIndices[programIndex] = foundProgramLocation;
			return true;
		}
		else{
			return false;
		}
	}


	public static double[] boundsToFeatures(int[][] bounds,int nFeaturesPerLine){
		double[] out = new double[0];

		for (int[] line:bounds){
			for (int index:line){
				double[] v = Utils.intToTernary(index, nFeaturesPerLine);
				out = Utils.cat(out, v);
			}
		}

		return out;
	}
	public static double[] programToFeatures(int[][] program,int nFeaturesPerLine,ArrayList<int[]> opts){
		double[] out = new double[0];

		System.out.println();
		for (int[] line:program) {
			for (int k:line) {
				System.out.print(k + " ");
			}
			System.out.println();
		}

		for (int[] line:program){
			int index = getIndexOfLine(line,opts);
			double[] v = Utils.intToTernary(index, nFeaturesPerLine);
			out = Utils.cat(out, v);
		}

		return out;
	}

	public static int getIndexOfLine(int[] lineData,ArrayList<int[]> maximalOpts){
		outer:
			for (int o=0;o<maximalOpts.size();o++) {
				int[] opt = maximalOpts.get(o);
				for (int k=0;k<lineData.length;k++) {
					if (lineData[k] != opt[k]) {
						continue outer;
					}
				}
				return o;
			}

	throw new Error("Op not found in maximal options: " + lineData[0] + " " +  lineData[1] + " " +  lineData[2] + " " +  lineData[3] + " " +  lineData[4]);
	}

	public static boolean areFunctionallyIdentical(int[][] program,int[][] mutant, int nVars, int nArrays,
			Gridlang interpreter) {
		Problem p = new PEasy_AddOne();
		int[] input;
		int param;
		int[] out;
		int[] out2;
		for (int t=0;t<25;t++) {
			input = p.randomInput(8);
			param = Utils.random.nextInt(4) + 1;
			out = interpreter.process(program, nArrays, nVars, input.clone(), param);
			out2 = interpreter.process(mutant, nArrays, nVars, input.clone(), param);
			for (int i=0;i<out.length;i++) {
//				System.out.println(out[i] + " " + out2[i]);
				if (out[i] != out2[i]) {return false;}
			}
		}
		return true;
	}
}
